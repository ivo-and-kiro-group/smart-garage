INSERT INTO smart_garage.users_credentials (users_credentials_id, email, password, reset_password_token) VALUES (34, 'ivayloangelov@gmail.com', '$2a$10$nLkFBTDvnIu4MDB/Gw93i.jaYIpJs2fNx2xGlnBUZScWSdVXmo6U6', null);
INSERT INTO smart_garage.users_credentials (users_credentials_id, email, password, reset_password_token) VALUES (35, 'kirilgeorgiev@gmail.com', '$2a$10$IGD/84V/E.4u5/.0E3SF2O1mYzYm8qMGw6xO09prS13hnzQSBqdTq', null);
INSERT INTO smart_garage.users_credentials (users_credentials_id, email, password, reset_password_token) VALUES (42, 'john.smith@gmail.com', '$2a$10$ZsrXb5H9MH4oGx2sur6n2e40oqux0PVDZ2mYW8ZJJEMR0Gbs8nOE6', null);
INSERT INTO smart_garage.users_credentials (users_credentials_id, email, password, reset_password_token) VALUES (49, 'd.ivanov@gmail.com', '$2a$10$raR0BW.f0iCSLmRLa9QnwupmaxBhBIhWxdluiPfsapFNVxaeFLnTa', null);
INSERT INTO smart_garage.users_credentials (users_credentials_id, email, password, reset_password_token) VALUES (51, 'atanas_tankov@gmail.com', '$2a$10$BjZiuC3m./eCOruOSIpyReSfqpzy9N0aP34UJIaElg85/5OdONM8S', null);

INSERT INTO smart_garage.users (user_id, first_name, last_name, phone, users_credentials_id) VALUES (29, 'Ivaylo', 'Angelov', '+359887334542', 34);
INSERT INTO smart_garage.users (user_id, first_name, last_name, phone, users_credentials_id) VALUES (30, 'Kiril', 'Geoegiev', '+359887334150', 35);
INSERT INTO smart_garage.users (user_id, first_name, last_name, phone, users_credentials_id) VALUES (37, 'John', 'Smith', '+44876127140', 42);
INSERT INTO smart_garage.users (user_id, first_name, last_name, phone, users_credentials_id) VALUES (44, 'David', 'Ivanov', '+44876127141', 49);
INSERT INTO smart_garage.users (user_id, first_name, last_name, phone, users_credentials_id) VALUES (46, 'Atanas', 'Tankov', '+44876127112', 51);

INSERT INTO smart_garage.customers (customer_id, user_id) VALUES (31, 37);
INSERT INTO smart_garage.customers (customer_id, user_id) VALUES (36, 44);
INSERT INTO smart_garage.customers (customer_id, user_id) VALUES (38, 46);

INSERT INTO smart_garage.employees (employee_id, user_id) VALUES (6, 29);
INSERT INTO smart_garage.employees (employee_id, user_id) VALUES (7, 30);

INSERT INTO smart_garage.roles (role_id, name) VALUES (3, 'ROLE_ADMIN');
INSERT INTO smart_garage.roles (role_id, name) VALUES (1, 'ROLE_CUSTOMER');
INSERT INTO smart_garage.roles (role_id, name) VALUES (2, 'ROLE_EMPLOYEE');

INSERT INTO smart_garage.users_roles (user_id, role_id) VALUES (29, 2);
INSERT INTO smart_garage.users_roles (user_id, role_id) VALUES (30, 2);
INSERT INTO smart_garage.users_roles (user_id, role_id) VALUES (29, 3);
INSERT INTO smart_garage.users_roles (user_id, role_id) VALUES (30, 3);
INSERT INTO smart_garage.users_roles (user_id, role_id) VALUES (37, 1);
INSERT INTO smart_garage.users_roles (user_id, role_id) VALUES (44, 1);
INSERT INTO smart_garage.users_roles (user_id, role_id) VALUES (46, 1);

INSERT INTO smart_garage.vehicle_types (type_id, name) VALUES (1, 'Car');
INSERT INTO smart_garage.vehicle_types (type_id, name) VALUES (2, 'Truck');

INSERT INTO smart_garage.statuses (status_id, name) VALUES (5, 'Finished');
INSERT INTO smart_garage.statuses (status_id, name) VALUES (3, 'In Process');
INSERT INTO smart_garage.statuses (status_id, name) VALUES (4, 'Preparing');
INSERT INTO smart_garage.statuses (status_id, name) VALUES (6, 'Ready for pick up');

INSERT INTO smart_garage.manufacturers (manufacturer_id, name) VALUES (1, 'Audi');
INSERT INTO smart_garage.manufacturers (manufacturer_id, name) VALUES (2, 'BMW');
INSERT INTO smart_garage.manufacturers (manufacturer_id, name) VALUES (3, 'Ferrari');
INSERT INTO smart_garage.manufacturers (manufacturer_id, name) VALUES (4, 'Mercedes');
INSERT INTO smart_garage.manufacturers (manufacturer_id, name) VALUES (5, 'Porsche');

INSERT INTO smart_garage.models (model_id, manufacturer_id, name) VALUES (1, 1, 'R8');
INSERT INTO smart_garage.models (model_id, manufacturer_id, name) VALUES (2, 2, 'X5');
INSERT INTO smart_garage.models (model_id, manufacturer_id, name) VALUES (3, 2, 'E90');
INSERT INTO smart_garage.models (model_id, manufacturer_id, name) VALUES (4, 1, 'Q7');
INSERT INTO smart_garage.models (model_id, manufacturer_id, name) VALUES (5, 3, 'F8 Spider');
INSERT INTO smart_garage.models (model_id, manufacturer_id, name) VALUES (6, 5, 'Carrera');
INSERT INTO smart_garage.models (model_id, manufacturer_id, name) VALUES (7, 4, 'S63');
INSERT INTO smart_garage.models (model_id, manufacturer_id, name) VALUES (8, 1, 'A6');
INSERT INTO smart_garage.models (model_id, manufacturer_id, name) VALUES (10, 1, 'RS6');
INSERT INTO smart_garage.models (model_id, manufacturer_id, name) VALUES (11, 2, 'E60');

INSERT INTO smart_garage.services (service_id, name, price) VALUES (2, 'Check Tires', 10.26);
INSERT INTO smart_garage.services (service_id, name, price) VALUES (3, 'Oil Change', 75);
INSERT INTO smart_garage.services (service_id, name, price) VALUES (4, 'Brake Repair and Replacement', 300);
INSERT INTO smart_garage.services (service_id, name, price) VALUES (7, 'Electrical Diagnostics', 50);
INSERT INTO smart_garage.services (service_id, name, price) VALUES (8, 'Wheel Alignment', 25);
INSERT INTO smart_garage.services (service_id, name, price) VALUES (9, 'Tune Up', 60);
INSERT INTO smart_garage.services (service_id, name, price) VALUES (10, 'Fuel System Repair', 89.8);
INSERT INTO smart_garage.services (service_id, name, price) VALUES (11, 'Exhaust System Repair', 97.2);
INSERT INTO smart_garage.services (service_id, name, price) VALUES (12, 'Air Conditioning A/C Repair', 55.3);
INSERT INTO smart_garage.services (service_id, name, price) VALUES (13, 'Engine Cooling System Maintenance', 150.7);

INSERT INTO smart_garage.vehicles (vehicle_id, license_plate, year, vin, model_id, vehicle_type_id, customer_id) VALUES (37, 'BP8073CA', 2021, 'WZY12MN98H123HJNA', 8, 1, 31);
INSERT INTO smart_garage.vehicles (vehicle_id, license_plate, year, vin, model_id, vehicle_type_id, customer_id) VALUES (40, 'M5060CB', 2021, 'JKI12MN98H123HJAA', 3, 1, 38);
INSERT INTO smart_garage.vehicles (vehicle_id, license_plate, year, vin, model_id, vehicle_type_id, customer_id) VALUES (41, 'M5505CB', 2021, 'ZXC12MN98H123HJNA', 11, 1, 38);
INSERT INTO smart_garage.vehicles (vehicle_id, license_plate, year, vin, model_id, vehicle_type_id, customer_id) VALUES (42, 'TX6060CA', 2020, 'TYU12MN98H123HJNK', 7, 1, 36);

INSERT INTO smart_garage.visits (visit_id, visit_date, vehicle_id, status_id, total_price) VALUES (94, '2021-04-21', 37, 4, 10.26);
INSERT INTO smart_garage.visits (visit_id, visit_date, vehicle_id, status_id, total_price) VALUES (97, '2021-04-01', 40, 5, 435);
INSERT INTO smart_garage.visits (visit_id, visit_date, vehicle_id, status_id, total_price) VALUES (98, '2021-04-21', 41, 3, 612.66);
INSERT INTO smart_garage.visits (visit_id, visit_date, vehicle_id, status_id, total_price) VALUES (99, '2021-04-22', 42, 4, 522.86);

INSERT INTO smart_garage.visits_services (visit_id, service_id) VALUES (94, 2);
INSERT INTO smart_garage.visits_services (visit_id, service_id) VALUES (97, 9);
INSERT INTO smart_garage.visits_services (visit_id, service_id) VALUES (97, 7);
INSERT INTO smart_garage.visits_services (visit_id, service_id) VALUES (97, 4);
INSERT INTO smart_garage.visits_services (visit_id, service_id) VALUES (97, 8);
INSERT INTO smart_garage.visits_services (visit_id, service_id) VALUES (98, 12);
INSERT INTO smart_garage.visits_services (visit_id, service_id) VALUES (98, 2);
INSERT INTO smart_garage.visits_services (visit_id, service_id) VALUES (98, 13);
INSERT INTO smart_garage.visits_services (visit_id, service_id) VALUES (98, 10);
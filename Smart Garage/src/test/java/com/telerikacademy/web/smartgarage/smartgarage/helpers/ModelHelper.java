package com.telerikacademy.web.smartgarage.smartgarage.helpers;

import com.telerikacademy.web.smartgarage.models.Manufacturer;
import com.telerikacademy.web.smartgarage.models.Model;

import static com.telerikacademy.web.smartgarage.smartgarage.helpers.ManufacturerHelper.createMockManufacturer;

public class ModelHelper {

    public static Model createMockModel() {
        var mockManufacturer= createMockManufacturer();
        var mockModel = new Model();
        mockModel.setId(1L);
        mockModel.setManufacturer(mockManufacturer);
        mockModel.setName("WRX STI");
        return mockModel;
    }

}

package com.telerikacademy.web.smartgarage.smartgarage.services;

import com.telerikacademy.web.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.models.Employee;
import com.telerikacademy.web.smartgarage.models.searchparameters.EmployeeSearchParameters;
import com.telerikacademy.web.smartgarage.models.searchparameters.ServiceSearchParameters;
import com.telerikacademy.web.smartgarage.repositories.contracts.EmployeeRepository;
import com.telerikacademy.web.smartgarage.services.EmployeeServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;

import static com.telerikacademy.web.smartgarage.smartgarage.helpers.EmployeeHelper.createMockEmployee;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
public class EmployeeServiceImplTests {

    @Mock
    EmployeeRepository mockRepository;

    @InjectMocks
    EmployeeServiceImpl service;

    @Test
    public void getAll_Should_Call_Repository() {
        EmployeeSearchParameters ssp = new EmployeeSearchParameters();

        when(mockRepository.getAll(ssp))
                .thenReturn(new ArrayList<>());

        service.getAll(ssp);

        verify(mockRepository, times(1)).getAll(ssp);
    }

    @Test
    public void getById_Should_Call_Repository_When_EmployeeExist() {
        when(mockRepository.getById(100L))
                .thenThrow(EntityNotFoundException.class);

        assertThrows(EntityNotFoundException.class,
                () -> service.getById(100L));
    }

    @Test
    public void getByEmail_Should_Call_Repository_When_EmployeeExist() {

        Mockito.when(mockRepository.getByEmail("mock@email"))
                .thenReturn(createMockEmployee());
        // Act
        Employee result = service.getByEmail("mock@email");

        // Assert
        Assertions.assertEquals(1, result.getId());
        Assertions.assertEquals("mock@email", result.getUser().getUserCredentials().getEmail());
        Assertions.assertEquals("MockPassword", result.getUser().getUserCredentials().getPassword());
        Assertions.assertEquals("MockFirstName", result.getUser().getFirstName());
        Assertions.assertEquals("MockLastName", result.getUser().getLastName());
    }

    @Test
    public void getByPhone_Should_Call_Repository_When_EmployeeExist() {

        Mockito.when(mockRepository.getByEmail("0878852258"))
                .thenReturn(createMockEmployee());
        // Act
        Employee result = service.getByEmail("0878852258");

        // Assert
        Assertions.assertEquals(1, result.getId());
        Assertions.assertEquals("mock@email", result.getUser().getUserCredentials().getEmail());
        Assertions.assertEquals("MockPassword", result.getUser().getUserCredentials().getPassword());
        Assertions.assertEquals("MockFirstName", result.getUser().getFirstName());
        Assertions.assertEquals("MockLastName", result.getUser().getLastName());
        Assertions.assertEquals("0878852258", result.getUser().getPhone());
    }

    @Test
    public void create_Should_Throw_Duplicate_When_EmailExist() {

        // Arrange
        Employee employee  = createMockEmployee();

        when(mockRepository.isEmployeeExisting(employee.getUser().getUserCredentials().getEmail(),
                "user.userCredentials.email",-1L)).thenReturn(true);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> service.create(employee));
    }

    @Test
    public void create_Should_Throw_Duplicate_When_PhoneExist() {

        // Arrange
        // Arrange
        Employee employee  = createMockEmployee();


        when(mockRepository.isEmployeeExisting(employee.getUser().getUserCredentials().getEmail(),
                "user.userCredentials.email",-1L)).thenReturn(false);

        when(mockRepository.isEmployeeExisting(employee.getUser().getPhone(),
                "user.phone",-1L)).thenReturn(true);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> service.create(employee));
    }

    @Test
    public void update_Should_Throw_Duplicate_When_EmailExist() {

        // Arrange
        Employee employee  = createMockEmployee();
        employee.setId(2L);

        when(mockRepository.isEmployeeExisting(employee.getUser().getUserCredentials().getEmail(),
                "user.userCredentials.email",employee.getId())).thenReturn(true);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> service.update(employee ));
    }

    @Test
    public void update_Should_Throw_Duplicate_When_PhoneExist() {

        // Arrange
        Employee employee  = createMockEmployee();

        when(mockRepository.isEmployeeExisting(employee.getUser().getUserCredentials().getEmail(),
                "user.userCredentials.email",employee.getId())).thenReturn(false);

        when(mockRepository.isEmployeeExisting(employee.getUser().getPhone(),
                "user.phone",employee.getId())).thenReturn(true);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> service.update(employee));
    }


/*    @Test
    public void delete_Should_Throw_Duplicate_When_EmployeeDoesntExist() {
        // Arrange
        Employee employee2=createMockEmployee();
        employee2.setId(2L);
        Employee employee3=createMockEmployee();
        employee2.setId(3L);


        when(mockRepository.delete(2L));

        // Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class, () -> service.delete(Mockito.anyLong()));

        when(mockRepository.getAll(new EmployeeSearchParameters())).thenReturn(new ArrayList<>());

        service.getAll(new EmployeeSearchParameters());

        verify(mockRepository, times(1)).getAll(new EmployeeSearchParameters());
    }*/
}
package com.telerikacademy.web.smartgarage.smartgarage.services;

import com.telerikacademy.web.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;

import com.telerikacademy.web.smartgarage.models.Manufacturer;
import com.telerikacademy.web.smartgarage.models.Vehicle;
import com.telerikacademy.web.smartgarage.models.searchparameters.ManufacturerSearchParameters;
import com.telerikacademy.web.smartgarage.repositories.contracts.ManufacturerRepository;
import com.telerikacademy.web.smartgarage.services.ManufacturerServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.InjectMocks;
import org.mockito.Mock;

import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.ArrayList;


import static com.telerikacademy.web.smartgarage.smartgarage.helpers.ManufacturerHelper.createMockManufacturer;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ManufacturerServiceImplTests {

    @Mock
    ManufacturerRepository mockRepository;

    @InjectMocks
    ManufacturerServiceImpl service;


    @Test
    public void getAll_Should_Call_Repository() {
        ManufacturerSearchParameters msp = new ManufacturerSearchParameters();

        when(mockRepository.getAll(msp)).thenReturn(new ArrayList<>());

        service.getAll(msp);

        verify(mockRepository, times(1)).getAll(msp);
    }

    @Test
    public void getById_Should_Call_Repository_When_EmployeeExist() {
        when(mockRepository.getById(100L))
                .thenThrow(EntityNotFoundException.class);

        assertThrows(EntityNotFoundException.class,
                () -> service.getById(100L));
    }

    @Test
    public void getByName_Should_Call_Repository_When_EmployeeExist() {
        Manufacturer manufacturer = createMockManufacturer();
        when(mockRepository.getByName(manufacturer.getName())).thenThrow(EntityNotFoundException.class);

        assertThrows(EntityNotFoundException.class,()-> service.getByName(manufacturer.getName()));
    }

    @Test
    public void create_Should_Not_Throw_Duplicate_When_Name_Not_Exist() {

        // Arrange
        Manufacturer manufacturer = createMockManufacturer();

        when(mockRepository.getByName(manufacturer.getName())).thenThrow(EntityNotFoundException.class);
        when(mockRepository.create(manufacturer)).thenReturn(manufacturer);
        // Act, Assert
        Assertions.assertEquals(manufacturer,service.create(manufacturer));
    }

    @Test
    public void create_Should_Throw_Duplicate_When_NameExist() {

        // Arrange
        Manufacturer manufacturer = createMockManufacturer();

        manufacturer.setId(2L);

        when(mockRepository.getByName(manufacturer.getName())).thenReturn(createMockManufacturer());

        Assertions.assertThrows(DuplicateEntityException.class,()-> service.update(manufacturer));
    }

    @Test
    public void update_Should_Not_Throw_Duplicate_When_Name_Not_Exist() {

        // Arrange
        Manufacturer manufacturer = createMockManufacturer();

        when(mockRepository.getByName(manufacturer.getName())).thenThrow(EntityNotFoundException.class);
        when(mockRepository.update(manufacturer)).thenReturn(manufacturer);
        // Act, Assert
        Assertions.assertEquals(manufacturer,service.update(manufacturer));
    }

    @Test
    public void update_Should_Throw_Duplicate_When_NameExist() {

        // Arrange
        Manufacturer manufacturer = createMockManufacturer();

        manufacturer.setId(2L);

        when(mockRepository.getByName(manufacturer.getName())).thenReturn(createMockManufacturer());

        Assertions.assertThrows(DuplicateEntityException.class,()-> service.update(manufacturer));
    }
/*
    @Test
    public void findPaginated_Should_Return_Pages() {
        Page<Manufacturer> page = service.findPaginated(PageRequest.of(1, 1));

        assertThat(page.getSize(), equalTo(1));
    }*/

    @Test
    public void validate_delete_manufacturer() {
        var mockManufacturer = createMockManufacturer();

        service.delete(1L);

        verify(mockRepository, times(1)).delete(mockManufacturer.getId());
    }
}

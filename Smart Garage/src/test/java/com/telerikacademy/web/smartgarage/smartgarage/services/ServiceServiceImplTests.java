package com.telerikacademy.web.smartgarage.smartgarage.services;

import com.telerikacademy.web.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.models.Service;
import com.telerikacademy.web.smartgarage.models.Vehicle;
import com.telerikacademy.web.smartgarage.models.Visit;
import com.telerikacademy.web.smartgarage.models.searchparameters.ServiceSearchParameters;
import com.telerikacademy.web.smartgarage.models.searchparameters.VehicleSearchParameters;
import com.telerikacademy.web.smartgarage.repositories.contracts.ServiceRepository;
import com.telerikacademy.web.smartgarage.services.ServiceServiceImpl;
import com.telerikacademy.web.smartgarage.smartgarage.helpers.ServiceHelper;
import com.telerikacademy.web.smartgarage.smartgarage.helpers.VehicleHelper;
import com.telerikacademy.web.smartgarage.smartgarage.helpers.VisitHelper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.ArrayList;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ServiceServiceImplTests {

    @Mock
    ServiceRepository repository;

    @InjectMocks
    ServiceServiceImpl service;

    @Test
    public void getAll_Should_Call_Repository() {

        ServiceSearchParameters ssp = new ServiceSearchParameters();

        when(repository.getAll(ssp))
                .thenReturn(new ArrayList<>());

        service.getAll(ssp);

        verify(repository, times(1)).getAll(ssp);
    }

    @Test
    public void getById_Should_Throw_WhenIdDoesNotExists() {
        when(repository.getById(100L))
                .thenThrow(EntityNotFoundException.class);

        assertThrows(EntityNotFoundException.class,
                () -> service.getById(100L));
    }

    @Test
    public void getById_Should_Call_Repository() {
        when(repository.getById(1L))
                .thenReturn(any(Service.class));

        service.getById(1L);

        verify(repository, times(1)).getById(1L);
    }

    @Test
    public void findPaginated_Should_Return_Pages() {
        Page<Service> page = service.findPaginated(PageRequest.of(1, 1));

        assertThat(page.getSize(), equalTo(1));
    }

    @Test
    public void getServicesByVisit_Should_ReturnListOfServices() {
        var mockVisit = VisitHelper.createMockVisit();

        when(repository.getServicesByVisit(mockVisit)).thenReturn(new ArrayList<>());

        service.getServicesByVisit(mockVisit);

        verify(repository, times(1)).getServicesByVisit(mockVisit);
    }

    @Test
    public void getByName_Should_Throw_When_GivenServiceDoesNotExist() {
        var mockService = ServiceHelper.createMockService();

        when(repository.getByName(mockService.getName())).thenThrow(EntityNotFoundException.class);

        assertThrows(EntityNotFoundException.class, () -> service.getByName(mockService.getName()));
    }

    @Test
    public void getByName_Should_Call_Repository_When_GivenServiceExists() {
        var mockService = ServiceHelper.createMockService();

        when(repository.getByName(mockService.getName())).thenReturn(any(Service.class));

        service.getByName(mockService.getName());

        verify(repository, times(1)).getByName(mockService.getName());
    }

    @Test
    public void create_Should_Throw_WhenServiceWithSameNameExists() {
        var mockService = ServiceHelper.createMockService();

        when(repository
                .isServiceExisting(mockService.getName(),
                        "name",
                        -1L))
                .thenReturn(true);

        assertThrows(DuplicateEntityException.class, () -> service.create(mockService));
    }

    @Test
    public void create_Should_Call_Repository_When_NameIsValid() {
        var mockService = ServiceHelper.createMockService();

        when(repository
                .isServiceExisting(mockService.getName(),
                        "name",
                        -1L))
                .thenReturn(false);

        service.create(mockService);

        verify(repository, times(1)).isServiceExisting(mockService.getName(),
                "name",
                -1L);
    }

    @Test
    public void update_Should_Throw_WhenServiceWithSameNameExists() {
        var mockService = ServiceHelper.createMockService();

        when(repository
                .isServiceExisting(mockService.getName(),
                        "name",
                        mockService.getId()))
                .thenReturn(true);

        assertThrows(DuplicateEntityException.class, () -> service.update(mockService));
    }

    @Test
    public void update_Should_Call_Repository_When_NameIsValid() {
        var mockService = ServiceHelper.createMockService();

        when(repository
                .isServiceExisting(mockService.getName(),
                        "name",
                        mockService.getId()))
                .thenReturn(false);

        service.update(mockService);

        verify(repository, times(1)).isServiceExisting(mockService.getName(),
                "name",
                mockService.getId());
    }

    @Test
    public void calcTotalPrice_Should_Return_DoubleValue() {
        var mockVisit = VisitHelper.createMockVisit();

        assertEquals(16, service.calcTotalPrice(mockVisit.getServices()));
    }

    @Test
    public void delete_Should_Call_Repository() {
        service.delete(anyLong());
        verify(repository, times(1)).delete(anyLong());
    }

}


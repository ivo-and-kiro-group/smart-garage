package com.telerikacademy.web.smartgarage.smartgarage.helpers;

import com.telerikacademy.web.smartgarage.models.*;

import java.util.Set;

public class CustomerHelper {

    public static Customer createMockCustomer() {
        var mockCustomer = new Customer();
        mockCustomer.setId(1L);
        mockCustomer.setUser(createMockUser());
        return mockCustomer;
    }

    public static User createMockUser() {
        var mockUser = new User();
        mockUser.setId(1L);
        mockUser.setUserCredentials(new UserCredentials());
        mockUser.getUserCredentials().setId(1L);
        mockUser.getUserCredentials().setEmail("mock@email");
        mockUser.getUserCredentials().setPassword("MockPassword");
        mockUser.setFirstName("MockFirstName");
        mockUser.setLastName("MockLastName");
        mockUser.setPhone("0878852258");
        mockUser.setRoles(Set.of(createMockRole()));
        return mockUser;
    }

    public static Role createMockRole() {
        var mockRole = new Role();
        mockRole.setName("ROLE_EMPLOYEE");
        mockRole.setId(1L);
        return mockRole;
    }

}

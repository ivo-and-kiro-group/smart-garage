package com.telerikacademy.web.smartgarage.smartgarage.helpers;

import com.telerikacademy.web.smartgarage.models.Service;

public class ServiceHelper {

    public static Service createMockService() {
        Service mockService = new Service();
        mockService.setId(1L);
        mockService.setName("mockServiceName");
        mockService.setPrice(10.5);
        return mockService;
    }
}

package com.telerikacademy.web.smartgarage.smartgarage.services;


import com.telerikacademy.web.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.models.Customer;
import com.telerikacademy.web.smartgarage.models.Customer;
import com.telerikacademy.web.smartgarage.models.searchparameters.CustomerSearchParameters;
import com.telerikacademy.web.smartgarage.models.searchparameters.CustomerSearchParameters;
import com.telerikacademy.web.smartgarage.repositories.contracts.CustomerRepository;
import com.telerikacademy.web.smartgarage.repositories.contracts.CustomerRepository;
import com.telerikacademy.web.smartgarage.services.CustomerServiceImpl;
import com.telerikacademy.web.smartgarage.services.CustomerServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;



import static com.telerikacademy.web.smartgarage.smartgarage.helpers.CustomerHelper.createMockCustomer;
import static com.telerikacademy.web.smartgarage.smartgarage.helpers.CustomerHelper.createMockCustomer;
import static com.telerikacademy.web.smartgarage.smartgarage.helpers.CustomerHelper.createMockCustomer;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CustomerServiceImplTests {

    @Mock
    CustomerRepository mockRepository;

    @InjectMocks
    CustomerServiceImpl service;

    @Test
    public void getAll_Should_Call_Repository() {
        CustomerSearchParameters ssp = new CustomerSearchParameters();

        when(mockRepository.getAll(ssp))
                .thenReturn(new ArrayList<>());

        service.getAll(ssp);

        verify(mockRepository, times(1)).getAll(ssp);
    }

    @Test
    public void getById_Should_Call_Repository_When_CustomerExist() {
        when(mockRepository.getById(100L))
                .thenThrow(EntityNotFoundException.class);

        assertThrows(EntityNotFoundException.class,
                () -> service.getById(100L));
    }

    @Test
    public void getByEmail_Should_Call_Repository_When_CustomerExist() {

        Mockito.when(mockRepository.getByEmail("mock@email"))
                .thenReturn(createMockCustomer());
        // Act
        Customer result = service.getByEmail("mock@email");

        // Assert
        Assertions.assertEquals(1, result.getId());
        Assertions.assertEquals("mock@email", result.getUser().getUserCredentials().getEmail());
        Assertions.assertEquals("MockPassword", result.getUser().getUserCredentials().getPassword());
        Assertions.assertEquals("MockFirstName", result.getUser().getFirstName());
        Assertions.assertEquals("MockLastName", result.getUser().getLastName());
    }

    @Test
    public void getByPhone_Should_Call_Repository_When_CustomerExist() {

        Mockito.when(mockRepository.getByEmail("0878852258"))
                .thenReturn(createMockCustomer());
        // Act
        Customer result = service.getByEmail("0878852258");

        // Assert
        Assertions.assertEquals(1, result.getId());
        Assertions.assertEquals("mock@email", result.getUser().getUserCredentials().getEmail());
        Assertions.assertEquals("MockPassword", result.getUser().getUserCredentials().getPassword());
        Assertions.assertEquals("MockFirstName", result.getUser().getFirstName());
        Assertions.assertEquals("MockLastName", result.getUser().getLastName());
        Assertions.assertEquals("0878852258", result.getUser().getPhone());
    }

    @Test
    public void create_Should_Throw_Duplicate_When_EmailExist() {

        // Arrange
        Customer customer = createMockCustomer();

        when(mockRepository.isCustomerExisting(customer.getUser().getUserCredentials().getEmail(),
                "user.userCredentials.email",-1L)).thenReturn(true);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> service.create(customer));
    }

    @Test
    public void create_Should_Throw_Duplicate_When_PhoneExist() {

        // Arrange
        // Arrange
        Customer customer  = createMockCustomer();


        when(mockRepository.isCustomerExisting(customer.getUser().getUserCredentials().getEmail(),
                "user.userCredentials.email",-1L)).thenReturn(false);

        when(mockRepository.isCustomerExisting(customer.getUser().getPhone(),
                "user.phone",-1L)).thenReturn(true);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> service.create(customer));
    }

    @Test
    public void update_Should_Throw_Duplicate_When_EmailExist() {

        // Arrange
        Customer customer  = createMockCustomer();
        customer.setId(2L);

        when(mockRepository.isCustomerExisting(customer.getUser().getUserCredentials().getEmail(),
                "user.userCredentials.email",customer.getId())).thenReturn(true);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> service.update(customer ));
    }

    @Test
    public void update_Should_Throw_Duplicate_When_PhoneExist() {

        // Arrange
        Customer customer  = createMockCustomer();

        when(mockRepository.isCustomerExisting(customer.getUser().getUserCredentials().getEmail(),
                "user.userCredentials.email",customer.getId())).thenReturn(false);

        when(mockRepository.isCustomerExisting(customer.getUser().getPhone(),
                "user.phone",customer.getId())).thenReturn(true);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> service.update(customer));
    }

}
package com.telerikacademy.web.smartgarage.smartgarage.services;

import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.exceptions.LocalDateException;
import com.telerikacademy.web.smartgarage.models.Visit;
import com.telerikacademy.web.smartgarage.models.searchparameters.VisitSearchParameters;
import com.telerikacademy.web.smartgarage.repositories.contracts.VisitRepository;
import com.telerikacademy.web.smartgarage.services.VisitServiceImpl;
import com.telerikacademy.web.smartgarage.services.contracts.ServiceService;
import com.telerikacademy.web.smartgarage.smartgarage.helpers.VisitHelper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import javax.mail.MessagingException;
import java.time.LocalDate;
import java.util.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class VisitServiceImplTests {

    @Mock
    VisitRepository repository;

    @InjectMocks
    VisitServiceImpl service;

    @InjectMocks
    ServiceServiceImplTests serviceServiceImpl;

    @Test
    public void getById_Should_Call_Repository() {
        when(repository.getById(1L))
                .thenReturn(any(Visit.class));

        service.getById(1L);

        verify(repository, times(1)).getById(1L);
    }

    @Test
    public void getById_Should_Throw_When_VisitDoesNotExist() {
        when(repository.getById(anyLong())).thenThrow(EntityNotFoundException.class);

        assertThrows(EntityNotFoundException.class, () -> service.getById(anyLong()));
    }

    @Test
    public void getAll_Should_Returns_ListOfVisits() {
        var mockVsp = new VisitSearchParameters();

        when(repository.getAll(mockVsp)).thenReturn(new ArrayList<Visit>());

        service.getAll(mockVsp);

        verify(repository, times(1)).getAll(mockVsp);
    }

    @Test
    public void getByDate_Should_Call_Repository_When_VisitExists() {
        var mockDate = VisitHelper.createMockVisit().getDate();

        when(repository.getByDate(mockDate)).thenReturn(any(Visit.class));

        service.getByDate(mockDate);

        verify(repository, times(1)).getByDate(mockDate);
    }

    @Test
    public void getByDate_Should_Throw_When_VisitDoesNotExist() {
        var mockDate = VisitHelper.createMockVisit().getDate();

        when(repository.getByDate(mockDate)).thenThrow(EntityNotFoundException.class);

        assertThrows(EntityNotFoundException.class, () -> service.getByDate(mockDate));
    }

    @Test
    public void create_Should_Throw_WhenDateIsInThePast() {
        var date = LocalDate.of(2020, 1, 1);
        var mockVisit = VisitHelper.createMockVisit();
        mockVisit.setDate(date);

        assertThrows(LocalDateException.class, () -> service.create(mockVisit));
    }

    @Test
    public void create_Should_Throw_When_DateIsInTheFuture() {
        var date = LocalDate.of(2022, 1, 1);
        var mockVisit = VisitHelper.createMockVisit();
        mockVisit.setDate(date);

        assertThrows(LocalDateException.class, () -> service.create(mockVisit));
    }

    @Test
    public void create_Should_Call_Repository_When_DateIsValid() {
        var mockVisit = VisitHelper.createMockVisit();

        when(repository.create(mockVisit)).thenReturn(any(Visit.class));

        service.create(mockVisit);

        verify(repository, times(1)).create(mockVisit);
    }

    @Test
    public void update_Should_Throw_WhenDateIsInThePast() {
        var date = LocalDate.of(2020, 1, 1);
        var mockVisit = VisitHelper.createMockVisit();
        mockVisit.setDate(date);

        assertThrows(LocalDateException.class, () -> service.update(mockVisit));
    }

    @Test
    public void update_Should_Throw_When_DateIsInTheFuture() {
        var date = LocalDate.of(2022, 1, 1);
        var mockVisit = VisitHelper.createMockVisit();
        mockVisit.setDate(date);

        assertThrows(LocalDateException.class, () -> service.update(mockVisit));
    }

    @Test
    public void update_Should_Call_Repository_When_DateIsValid() throws MessagingException {
        var mockVisit = VisitHelper.createMockVisit();

        when(repository.update(mockVisit)).thenReturn(mockVisit);

        service.update(mockVisit);

        verify(repository, times(1)).update(mockVisit);
    }

    @Test
    public void delete_Should_Call_Repository_When_Visit_Exists() {

        service.delete(anyLong());

        verify(repository, times(1)).delete(anyLong());
    }

//    @Test
//    public void findPaginated_Should_Return_Pages() {
//        Page<Visit> page = service.findPaginated(PageRequest.of(1, 1));
//
//        assertThat(page.getSize(), equalTo(1));
//    }


}

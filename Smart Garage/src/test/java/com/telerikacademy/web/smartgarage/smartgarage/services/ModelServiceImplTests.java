package com.telerikacademy.web.smartgarage.smartgarage.services;

import com.telerikacademy.web.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.models.Manufacturer;
import com.telerikacademy.web.smartgarage.models.Model;
import com.telerikacademy.web.smartgarage.models.searchparameters.ManufacturerSearchParameters;
import com.telerikacademy.web.smartgarage.models.searchparameters.ModelSearchParameters;
import com.telerikacademy.web.smartgarage.repositories.contracts.ManufacturerRepository;
import com.telerikacademy.web.smartgarage.repositories.contracts.ModelRepository;
import com.telerikacademy.web.smartgarage.services.ManufacturerServiceImpl;
import com.telerikacademy.web.smartgarage.services.ModelServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static com.telerikacademy.web.smartgarage.smartgarage.helpers.ManufacturerHelper.createMockManufacturer;
import static com.telerikacademy.web.smartgarage.smartgarage.helpers.ModelHelper.createMockModel;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
public class ModelServiceImplTests {
    @Mock
    ModelRepository mockRepository;

    @InjectMocks
    ModelServiceImpl service;


    @Test
    public void getAll_Should_Call_Repository() {
        ModelSearchParameters msp = new ModelSearchParameters();

        when(mockRepository.getAll(msp)).thenReturn(new ArrayList<>());

        service.getAll(msp);

        verify(mockRepository, times(1)).getAll(msp);
    }

    @Test
    public void getById_Should_Call_Repository_When_EmployeeExist() {
        when(mockRepository.getById(100L))
                .thenThrow(EntityNotFoundException.class);

        assertThrows(EntityNotFoundException.class,
                () -> service.getById(100L));
    }


    @Test
    public void getByName_Should_Call_Repository_When_EmployeeExist() {
        Model model = createMockModel();
        when(mockRepository.getByName(model.getName())).thenThrow(EntityNotFoundException.class);

        assertThrows(EntityNotFoundException.class,()-> service.getByName(model.getName()));
    }

    @Test
    public void create_Should_Not_Throw_Duplicate_When_Name_Not_Exist() {

        // Arrange
        Model model = createMockModel();

        when(mockRepository.getByName(model.getName())).thenThrow(EntityNotFoundException.class);
        when(mockRepository.create(model)).thenReturn(model);
        // Act, Assert
        Assertions.assertEquals(model,service.create(model));
    }

    @Test
    public void create_Should_Throw_Duplicate_When_NameExist() {

        // Arrange
        Model model = createMockModel();

        model.setId(2L);

        when(mockRepository.getByName(model.getName())).thenReturn(model);

        Assertions.assertThrows(DuplicateEntityException.class,()-> service.create(model));
    }

    @Test
    public void update_Should_Not_Throw_Duplicate_When_Name_Not_Exist() {

        // Arrange
        Model model = createMockModel();

        when(mockRepository.getByName(model.getName())).thenThrow(EntityNotFoundException.class);
        when(mockRepository.update(model)).thenReturn(model);
        // Act, Assert
        Assertions.assertEquals(model,service.update(model));
    }

    @Test
    public void update_Should_Throw_Duplicate_When_NameExist() {

        // Arrange
        Model model = createMockModel();
        model.setId(2L);

        when(mockRepository.getByName(model.getName())).thenReturn(createMockModel());

        Assertions.assertThrows(DuplicateEntityException.class,()-> service.update(model));
    }

    @Test
    public void validate_delete_manufacturer() {
        var mockManufacturer = createMockManufacturer();

        service.delete(1L);

        verify(mockRepository, times(1)).delete(mockManufacturer.getId());
    }
}

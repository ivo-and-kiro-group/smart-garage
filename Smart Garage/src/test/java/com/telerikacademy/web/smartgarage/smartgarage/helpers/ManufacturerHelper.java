package com.telerikacademy.web.smartgarage.smartgarage.helpers;

import com.telerikacademy.web.smartgarage.models.Manufacturer;

public class ManufacturerHelper {
    public static Manufacturer createMockManufacturer() {
        var mockManufacturer= new Manufacturer();
        mockManufacturer.setId(1L);
        mockManufacturer.setName("Subaru");
        return mockManufacturer;
    }
}

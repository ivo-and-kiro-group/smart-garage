package com.telerikacademy.web.smartgarage.smartgarage.helpers;

import com.telerikacademy.web.smartgarage.models.Manufacturer;
import com.telerikacademy.web.smartgarage.models.Model;
import com.telerikacademy.web.smartgarage.models.Vehicle;
import com.telerikacademy.web.smartgarage.models.VehicleType;

public class VehicleHelper {

    public static Vehicle createMockVehicle() {
        var mockVehicle = new Vehicle();
        mockVehicle.setId(1L);
        mockVehicle.setVehicleType(createMockVehicleType());
        mockVehicle.setVin("9832Hj73WW12v8j12");
        mockVehicle.setYear(2021L);
        mockVehicle.setModel(createMockModel());
        mockVehicle.setLicensePlate("BP8037CA");
        return mockVehicle;
    }

    public static VehicleType createMockVehicleType() {
        var mockVehicleType = new VehicleType();
        mockVehicleType.setId(1L);
        mockVehicleType.setName("Car");
        return mockVehicleType;
    }

    public static Model createMockModel() {
        var mockModel = new Model();
        mockModel.setId(1L);
        mockModel.setName("mockModel");
        mockModel.setManufacturer(createMockManufacturer());
        return mockModel;
    }

    public static Manufacturer createMockManufacturer() {
        var mockManufacturer = new Manufacturer();
        mockManufacturer.setId(1L);
        mockManufacturer.setName("mockManufacturer");
        return mockManufacturer;
    }

}

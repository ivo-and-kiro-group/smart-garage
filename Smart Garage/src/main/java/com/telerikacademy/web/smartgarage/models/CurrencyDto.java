package com.telerikacademy.web.smartgarage.models;

public class CurrencyDto {

    private String name;

    public CurrencyDto(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

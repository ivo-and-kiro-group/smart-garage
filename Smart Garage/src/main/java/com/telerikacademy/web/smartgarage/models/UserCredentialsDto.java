package com.telerikacademy.web.smartgarage.models;

import com.telerikacademy.web.smartgarage.constraint.FieldMatch;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@FieldMatch.List({
        @FieldMatch(first = "password", second = "confirmPassword", message = "The password fields must match"),
        @FieldMatch(first = "email", second = "confirmEmail", message = "The email fields must match")
})
public class UserCredentialsDto {
    @NotNull(message = "Email cannot be null.")
    @Email(message = "Invalid email")
    private String email;

    @NotNull(message = "Email cannot be null.")
    @Email(message = "Invalid email")
    private String confirmEmail;

    @Size(min = 2, max = 20, message = "Password should be between 2 and 20 symbols")
    private String password;

    @Size(min = 2, max = 20, message = "Password should be between 2 and 20 symbols")
    private String confirmPassword;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getConfirmEmail() {
        return confirmEmail;
    }

    public void setConfirmEmail(String confirmEmail) {
        this.confirmEmail = confirmEmail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }
}

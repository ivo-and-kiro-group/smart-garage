package com.telerikacademy.web.smartgarage.repositories;

import com.telerikacademy.web.smartgarage.models.VehicleType;
import com.telerikacademy.web.smartgarage.repositories.contracts.VehicleTypeRepository;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class VehicleTypeRepositoryImpl extends AbstractGenericGetRepository<VehicleType>
        implements VehicleTypeRepository {

    public VehicleTypeRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public List<VehicleType> getAll() {
        return super.getAll(VehicleType.class);
    }

    @Override
    public VehicleType getById(Long id) {
        return super.getByField("id", id, VehicleType.class);
    }
}

package com.telerikacademy.web.smartgarage.repositories.contracts;

import com.telerikacademy.web.smartgarage.models.Employee;
import com.telerikacademy.web.smartgarage.models.searchparameters.EmployeeSearchParameters;

import java.util.List;

public interface EmployeeRepository {
    List<Employee> getAll (EmployeeSearchParameters employeeSearchParameters);

    Employee getById(Long id);

    Employee getByEmail(String email);

    Employee getByPhone(String phone);

    Employee create(Employee employee);

    Employee update(Employee employee);

    void delete(Long id);

    boolean isEmployeeExisting(String fieldValue,String fieldName, Long id);
}

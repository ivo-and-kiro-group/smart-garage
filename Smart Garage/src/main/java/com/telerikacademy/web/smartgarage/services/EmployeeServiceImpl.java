package com.telerikacademy.web.smartgarage.services;

import com.telerikacademy.web.smartgarage.config.SecurityConfig;
import com.telerikacademy.web.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.web.smartgarage.models.Employee;
import com.telerikacademy.web.smartgarage.models.Role;
import com.telerikacademy.web.smartgarage.models.searchparameters.EmployeeSearchParameters;
import com.telerikacademy.web.smartgarage.repositories.contracts.EmployeeRepository;
import com.telerikacademy.web.smartgarage.services.contracts.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeServiceImpl implements EmployeeService  {
    private final EmployeeRepository repository;

    @Autowired
    public EmployeeServiceImpl(EmployeeRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Employee> getAll(EmployeeSearchParameters employeeSearchParameters){
        return repository.getAll(employeeSearchParameters);
    }

    @Override
    public Employee getById(Long id) {
        return repository.getById(id);
    }

    @Override
    public Employee getByPhone(String phone) {
        return repository.getByPhone(phone);
    }

    @Override
    public Employee getByEmail(String email) {
        return repository.getByEmail(email);
    }

    @Override
    public Employee create(Employee employee) {
        validateDuplicate(employee,-1L);
        encodePassword(employee);
        return repository.create(employee);
    }

    @Override
    public Employee update(Employee employee) {
        validateDuplicate(employee,employee.getId());
        if (employee.getUser().getUserCredentials().getPassword() != null
                && !employee.getUser().getUserCredentials().getPassword().equals("")
                && employee.getUser().getUserCredentials().getPassword().length()>20) {
            encodePassword(employee);
        }
        return repository.update(employee);
    }

    @Override
    public void delete(Long id) {
        repository.delete(id);
    }

    @Override
    public Page<Employee> findPaginated(Pageable pageable, EmployeeSearchParameters employeeSearchParameters) {

        List<Employee> allEmployees = getAll(employeeSearchParameters);

        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<Employee> list;

        if (allEmployees.size()< startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, allEmployees.size());
            list = allEmployees.subList(startItem, toIndex);
        }

        return new PageImpl<Employee>(list, PageRequest.of(currentPage, pageSize), allEmployees.size());
    }

    private void encodePassword(Employee employee) {
        employee.getUser().getUserCredentials().setPassword(
                SecurityConfig.passwordEncoder().encode(employee.getUser().getUserCredentials().getPassword())
        );
    }

    private void validateDuplicate(Employee employee,Long id){
        if (repository.isEmployeeExisting(employee.getUser().getUserCredentials().getEmail(),"user.userCredentials.email",id)) {
            throw new DuplicateEntityException("Employee", "email", employee.getUser().getUserCredentials().getEmail());
        }

        if (repository.isEmployeeExisting(employee.getUser().getPhone(),"user.phone",id)) {
            throw new DuplicateEntityException("Employee", "phone", employee.getUser().getPhone());
        }
    }
}

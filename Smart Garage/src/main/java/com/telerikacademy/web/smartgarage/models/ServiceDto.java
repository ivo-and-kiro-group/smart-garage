package com.telerikacademy.web.smartgarage.models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class ServiceDto {

    @NotNull(message = "Service's name cannot be null.")
    @Size(min = 5, max = 50, message = "Service's name should be between 5 and 50 symbols.")
    private String name;

    @NotNull(message = "Service's price cannot be null.")
    @Positive(message = "Service's price should be positive.")
    private Double price;

    public ServiceDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}

package com.telerikacademy.web.smartgarage.controllers.rest;


import com.telerikacademy.web.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;

import com.telerikacademy.web.smartgarage.models.*;
import com.telerikacademy.web.smartgarage.models.response.JwtResponse;
import com.telerikacademy.web.smartgarage.repositories.contracts.RoleRepository;
import com.telerikacademy.web.smartgarage.securityjwt.JwtProvider;
import com.telerikacademy.web.smartgarage.services.contracts.CustomerService;
import com.telerikacademy.web.smartgarage.modelmapper.CustomerModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.mail.MessagingException;
import javax.validation.Valid;
import java.io.UnsupportedEncodingException;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthRestAPIs {

    private final AuthenticationManager authenticationManager;

    private final CustomerService customerService;

    private final RoleRepository roleRepository;

    private final PasswordEncoder encoder;

    private final JwtProvider jwtProvider;

    private final CustomerModelMapper customerModelMapper;

    @Autowired
    public AuthRestAPIs(AuthenticationManager authenticationManager, CustomerService customerService, RoleRepository roleRepository,
                        PasswordEncoder encoder,
                        JwtProvider jwtProvider,
                        CustomerModelMapper customerModelMapper) {
        this.authenticationManager = authenticationManager;
        this.customerService = customerService;
        this.roleRepository = roleRepository;
        this.encoder = encoder;
        this.jwtProvider = jwtProvider;
        this.customerModelMapper = customerModelMapper;
    }

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginForm loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsername(),
                        loginRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = jwtProvider.generateJwtToken(authentication);
        return ResponseEntity.ok(new JwtResponse(jwt));
    }

    @PostMapping("/signup")
    public Customer registerUser(@Valid @RequestBody CustomerDto customerDto) {

        try {
            Customer customer = customerModelMapper.fromDto(customerDto);
            return customerService.create(customer);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (MessagingException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnsupportedEncodingException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }
}
package com.telerikacademy.web.smartgarage.repositories.contracts;


import com.telerikacademy.web.smartgarage.models.Customer;
import com.telerikacademy.web.smartgarage.models.Vehicle;
import com.telerikacademy.web.smartgarage.models.Visit;
import com.telerikacademy.web.smartgarage.models.searchparameters.CustomerSearchParameters;

import java.util.List;

public interface CustomerRepository {
    List<Customer> getAll(CustomerSearchParameters customerSearchParameters);

    Customer getById(Long id);

    Customer getByEmail(String email);

    Customer findByResetPassword(String token);

    Customer getByPhone(String phone);

    Customer create(Customer customer);

    Customer update(Customer customer);

    void delete(Long id);

    boolean isCustomerExisting(String email, String fieldName, Long id);

}

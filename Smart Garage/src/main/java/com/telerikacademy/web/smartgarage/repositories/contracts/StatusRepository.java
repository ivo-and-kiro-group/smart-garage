package com.telerikacademy.web.smartgarage.repositories.contracts;

import com.telerikacademy.web.smartgarage.models.Status;

import java.util.List;

public interface StatusRepository {

    List<Status> getAll();

    Status getById(Long id);
}

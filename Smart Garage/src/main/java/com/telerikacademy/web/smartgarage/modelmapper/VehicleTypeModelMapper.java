package com.telerikacademy.web.smartgarage.modelmapper;

import com.telerikacademy.web.smartgarage.models.VehicleType;
import com.telerikacademy.web.smartgarage.models.VehicleTypeDto;
import com.telerikacademy.web.smartgarage.repositories.contracts.VehicleTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class VehicleTypeModelMapper {

    private final VehicleTypeRepository repository;

    @Autowired
    public VehicleTypeModelMapper(VehicleTypeRepository repository) {
        this.repository = repository;
    }

    public VehicleType fromDto(VehicleTypeDto vehicleTypeDto) {
        VehicleType vehicleType = new VehicleType();
        dtoToObject(vehicleTypeDto, vehicleType);
        return vehicleType;
    }

    public VehicleType fromDto(VehicleTypeDto vehicleTypeDto, Long id) {
        VehicleType vehicleType = repository.getById(id);
        dtoToObject(vehicleTypeDto, vehicleType);
        return vehicleType;
    }

    private void dtoToObject(VehicleTypeDto vehicleTypeDto, VehicleType vehicleType) {
        vehicleType.setName(vehicleTypeDto.getName());
    }
}

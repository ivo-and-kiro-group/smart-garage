package com.telerikacademy.web.smartgarage.controllers.mvc;

import com.telerikacademy.web.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.exceptions.LocalDateException;
import com.telerikacademy.web.smartgarage.models.*;
import com.telerikacademy.web.smartgarage.models.searchparameters.ServiceSearchParameters;
import com.telerikacademy.web.smartgarage.models.searchparameters.VehicleSearchParameters;
import com.telerikacademy.web.smartgarage.services.contracts.ServiceService;
import com.telerikacademy.web.smartgarage.modelmapper.ServiceModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
@RequestMapping("/services")
public class ServiceMvcController {

    private final ServiceService service;
    private final ServiceModelMapper modelMapper;

    @Autowired
    public ServiceMvcController(ServiceService service,
                                ServiceModelMapper modelMapper) {
        this.service = service;
        this.modelMapper = modelMapper;
    }

    @GetMapping
    public String showAllServices(Model model,
                                  @RequestParam("page") Optional<Integer> page,
                                  @RequestParam("size") Optional<Integer> size) {

        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);

        Page<Service> servicePage = service.findPaginated(PageRequest.of(currentPage - 1, pageSize));
        List<Service> serviceList = servicePage.getContent();

        model.addAttribute("serviceList", serviceList);
        model.addAttribute("serviceSearchParameters", new ServiceSearchParameters());
        model.addAttribute("servicePage", servicePage);

        int totalPages = servicePage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }

        return "services";
    }

    @GetMapping("/create")
    @PreAuthorize("hasRole('EMPLOYEE')")
    public String showCreateService(Model model) {
        model.addAttribute("service", new ServiceDto());
        return "service-new";
    }


    @PostMapping("/create")
    public String handleCreateServicePage(@Valid @ModelAttribute("service") ServiceDto serviceDto,
                                          BindingResult errors) {

        if (errors.hasErrors()) {
            return "service-new";
        }

        try {
            Service serviceToCreate = modelMapper.fromDto(serviceDto);
            service.create(serviceToCreate);
            return "redirect:/services";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("name", "duplicated_name", e.getMessage());
            return "service-new";
        }
    }

    @GetMapping("/{id}/update")
    @PreAuthorize("hasRole('EMPLOYEE')")
    public String showEditService(@PathVariable Long id, Model model) {
        try {
            Service serviceToUpdate = service.getById(id);
            ServiceDto serviceDto = modelMapper.toDto(serviceToUpdate);
            model.addAttribute("service", serviceDto);
            return "service-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @PostMapping("/{id}/update")
    public String editService(@PathVariable Long id,
                              @Valid @ModelAttribute("service") ServiceDto serviceDto,
                              BindingResult errors) {

        if (errors.hasErrors()) {
            return "service-update";
        }

        try {
            Service serviceToUpdate = modelMapper.fromDto(serviceDto, id);
            service.update(serviceToUpdate);
            return "redirect:/services";
        } catch (LocalDateException e) {
            errors.rejectValue("name", "Invalid_name", e.getMessage());
            return "service-update";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("name", "duplicated_name", e.getMessage());
            return "service-update";
        }
    }

    @GetMapping("/search")
    public String handleSearchServices() {
        return "redirect:/services";
    }

    @PostMapping("/search")
    public String handleServiceSearch(Model model,
                                       @ModelAttribute("serviceSearchParameters")
                                               ServiceSearchParameters serviceSearchParameters ) {

        model.addAttribute("serviceList", service.getAll(serviceSearchParameters));

        return "services";
    }


    @GetMapping("/{id}/delete")
    @PreAuthorize("hasRole('EMPLOYEE')")
    public String deleteService(@PathVariable Long id, Model model) {
        try {
            service.delete(id);
            return "redirect:/services";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }
}

package com.telerikacademy.web.smartgarage.repositories.contracts;

import com.telerikacademy.web.smartgarage.models.VehicleType;

import java.util.List;

public interface VehicleTypeRepository {

    List<VehicleType> getAll();

    VehicleType getById(Long id);
}

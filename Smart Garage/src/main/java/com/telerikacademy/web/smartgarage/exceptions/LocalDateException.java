package com.telerikacademy.web.smartgarage.exceptions;

public class LocalDateException extends RuntimeException {
    public LocalDateException(String message) {
        super(message);
    }
}

package com.telerikacademy.web.smartgarage.models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class VehicleTypeDto {

    @NotNull(message = "Vehicle's type cannot be null.")
    @Size(min = 5, max = 20, message = "Vehicle's type should be between 5 and 20 symbols.")
    private String name;

    public VehicleTypeDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

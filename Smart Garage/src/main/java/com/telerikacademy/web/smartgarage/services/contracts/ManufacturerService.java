package com.telerikacademy.web.smartgarage.services.contracts;

import com.telerikacademy.web.smartgarage.models.Manufacturer;
import com.telerikacademy.web.smartgarage.models.searchparameters.ManufacturerSearchParameters;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ManufacturerService {
    List<Manufacturer> getAll(ManufacturerSearchParameters manufacturerSearchParameters);

    Manufacturer getById(Long id);

    Manufacturer getByName(String name);

    Manufacturer create(Manufacturer manufacturer);

    Manufacturer update(Manufacturer manufacturer);

    void delete(Long id);

    Page<Manufacturer> findPaginated (Pageable pageable,ManufacturerSearchParameters manufacturerSearchParameters);
}

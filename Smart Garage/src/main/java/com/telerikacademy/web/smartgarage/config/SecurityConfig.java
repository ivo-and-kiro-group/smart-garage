package com.telerikacademy.web.smartgarage.config;

import com.telerikacademy.web.smartgarage.securityjwt.JwtAuthEntryPoint;
import com.telerikacademy.web.smartgarage.securityjwt.JwtAuthTokenFilter;
import com.telerikacademy.web.smartgarage.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@SpringBootApplication
public class SecurityConfig {

    @Bean
    public static PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @EnableWebSecurity
    @EnableGlobalMethodSecurity(prePostEnabled = true)
    @Order(1)
    public static class WebSecurityConfig extends WebSecurityConfigurerAdapter {

        private final UserService userService;
        private final JwtAuthEntryPoint unauthorizedHandler;

        @Autowired
        public WebSecurityConfig(UserService userService, JwtAuthEntryPoint unauthorizedHandler) {
            this.userService = userService;
            this.unauthorizedHandler = unauthorizedHandler;
        }

        @Bean
        public JwtAuthTokenFilter authenticationJwtTokenFilter() {
            return new JwtAuthTokenFilter();
        }

        @Override
        public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
            authenticationManagerBuilder
                    .userDetailsService(userService)
                    .passwordEncoder(SecurityConfig.passwordEncoder());
        }

        @Bean
        @Override
        public AuthenticationManager authenticationManagerBean() throws Exception {
            return super.authenticationManagerBean();
        }


        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.antMatcher("/api/**").cors().and().csrf().disable().
                    authorizeRequests()
                    .antMatchers("/api/**").permitAll()
                    .anyRequest().authenticated()
//                .antMatchers("/admin/**").hasRole("ADMIN")
//                .anyRequest().authenticated()
                    .and()
                    .exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
                    .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                    .and()
                    .addFilterBefore(authenticationJwtTokenFilter(), UsernamePasswordAuthenticationFilter.class);


        }
    }

    @EnableWebSecurity
    @EnableGlobalMethodSecurity(prePostEnabled = true)
    public static class SecurityConfiguration extends WebSecurityConfigurerAdapter {
        /**
         * A common problem with using PrePost annotations on controllers is that Spring method security is based on Spring AOP, which is by default implemented
         * with JDK proxies.
         * <p>
         * That means that it works fine on the service layer which is injected in controller layer as interfaces, but it is ignored on controller layer
         * because controller generally do not implement interfaces.
         * <p>
         * The following is just my opinion:
         * <p>
         * prefered way: move the pre post annotation on service layer
         * if you cannot (or do not want to), try to have your controller implement an interface containing all the annotated methods
         * as a last way, use proxy-target-class=true
         * <p>
         * or use the annotations in the top
         */


        private final UserService userService;

        @Autowired
        public SecurityConfiguration(UserService userService) {
            this.userService = userService;
        }

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http
                    .authorizeRequests()
                    .antMatchers(
                           "/**" ,"/registration**",
                            "/js/**",
                            "/css/**",
                            "/assets**",
                            "/webjars/**").permitAll()
                    .anyRequest().authenticated()
                    .and()
                    .formLogin()
                    .loginPage("/login")
                    .permitAll()
                    .and()
                    .logout()
                    .invalidateHttpSession(true)
                    .clearAuthentication(true)
                    .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                    .logoutSuccessUrl("/")
                    .permitAll();
        }

        @Bean
        public DaoAuthenticationProvider authenticationProvider() {
            DaoAuthenticationProvider auth = new DaoAuthenticationProvider();
            auth.setUserDetailsService(userService);
            auth.setPasswordEncoder(SecurityConfig.passwordEncoder());
            return auth;
        }

        @Override
        protected void configure(AuthenticationManagerBuilder auth) throws Exception {
            auth.authenticationProvider(authenticationProvider());
        }

    }

}

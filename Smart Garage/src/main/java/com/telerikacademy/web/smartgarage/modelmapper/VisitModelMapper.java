package com.telerikacademy.web.smartgarage.modelmapper;

import com.telerikacademy.web.smartgarage.models.*;
import com.telerikacademy.web.smartgarage.repositories.contracts.ServiceRepository;
import com.telerikacademy.web.smartgarage.repositories.contracts.StatusRepository;
import com.telerikacademy.web.smartgarage.repositories.contracts.VehicleRepository;
import com.telerikacademy.web.smartgarage.repositories.contracts.VisitRepository;
import com.telerikacademy.web.smartgarage.services.contracts.ServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class VisitModelMapper {

    private final VisitRepository repository;
    private final StatusRepository statusRepository;
    private final VehicleRepository vehicleRepository;
    private final ServiceRepository serviceRepository;
    private final ServiceService serviceService;

    @Autowired
    public VisitModelMapper(VisitRepository repository,
                            StatusRepository statusRepository,
                            VehicleRepository vehicleRepository,
                            ServiceRepository serviceRepository,
                            ServiceService serviceService) {
        this.repository = repository;
        this.statusRepository = statusRepository;
        this.vehicleRepository = vehicleRepository;
        this.serviceRepository = serviceRepository;
        this.serviceService = serviceService;
    }

    public VisitDto toDto(Visit visit) {
        VisitDto visitDto = new VisitDto();
        visitDto.setDate(visit.getDate());
        visitDto.setStatusId(visit.getStatus().getId());
        visitDto.setLicensePlate(visit.getVehicle().getLicensePlate());

        Set<Long> services = new HashSet<>();

        for (Service service : serviceService.getServicesByVisit(visit)) {
            services.add(service.getId());
        }

        visitDto.setServices(services);
        return visitDto;
    }

    public Visit fromDto(VisitDto visitDto) {
        Visit visit = new Visit();
        dtoToObject(visitDto, visit);
        return visit;
    }

    public Visit fromDto(VisitDto visitDto, Long id) {
        Visit visit = repository.getById(id);
        dtoToObject(visitDto, visit);
        return visit;
    }

    private void dtoToObject(VisitDto visitDto, Visit visit) {
        Vehicle vehicle = vehicleRepository.getByLicensePlate(visitDto.getLicensePlate());
        Status status = statusRepository.getById(visitDto.getStatusId());

        visit.setDate(visitDto.getDate());
        visit.setVehicle(vehicle);
        visit.setStatus(status);
        visit.setTotalPrice(0.0);
        Set<Service> services = new HashSet<>();

        for (Long v : visitDto.getServices()) {
            services.add(serviceRepository.getById(v));
        }

        visit.setServices(services);
    }

}

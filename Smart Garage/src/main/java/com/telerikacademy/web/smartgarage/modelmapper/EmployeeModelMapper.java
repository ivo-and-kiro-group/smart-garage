package com.telerikacademy.web.smartgarage.modelmapper;

import com.telerikacademy.web.smartgarage.models.Employee;
import com.telerikacademy.web.smartgarage.models.EmployeeDto;
import com.telerikacademy.web.smartgarage.repositories.contracts.EmployeeRepository;
import com.telerikacademy.web.smartgarage.repositories.contracts.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class EmployeeModelMapper {
    private final EmployeeRepository employeeRepository;
    private final UserModelMapper userModelMapper;
    private final RoleRepository roleRepository;

    @Autowired
    public EmployeeModelMapper(EmployeeRepository employeeRepository, UserModelMapper userModelMapper, RoleRepository roleRepository) {
        this.employeeRepository = employeeRepository;
        this.userModelMapper = userModelMapper;
        this.roleRepository = roleRepository;
    }

    public Employee fromDto(EmployeeDto employeeDto) {
        Employee employee = new Employee();
        dtoToObject(employeeDto, employee);
        return employee;
    }

    public Employee fromDto(EmployeeDto employeeDto, Long id) {
        Employee employee = employeeRepository.getById(id);
        dtoToObjectUpdate(employeeDto, employee);
        return employee;
    }

    public EmployeeDto toDto(Employee employee) {
        EmployeeDto employeeDto = new EmployeeDto();
        employeeDto.setUser(userModelMapper.toDto(employee.getUser()));
        return employeeDto;
    }

    private void dtoToObject(EmployeeDto employeeDto, Employee employee) {
        employee.setUser(userModelMapper.fromDto(employeeDto.getUser()));
        employee.getUser().setRoles(Set.of(roleRepository.getByName("ROLE_EMPLOYEE")));
    }

    private void dtoToObjectUpdate(EmployeeDto employeeDto, Employee employee) {
        employee.setUser(userModelMapper.fromDto(employeeDto.getUser(), employee.getUser().getId()));
    }
}

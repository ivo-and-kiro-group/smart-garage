package com.telerikacademy.web.smartgarage.repositories;

import com.telerikacademy.web.smartgarage.models.Status;
import com.telerikacademy.web.smartgarage.repositories.contracts.StatusRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class StatusRepositoryImpl extends AbstractGenericGetRepository<Status> implements StatusRepository {

    @Autowired
    public StatusRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public List<Status> getAll() {
        return super.getAll(Status.class);
    }

    @Override
    public Status getById(Long id) {
        return super.getByField("id", id, Status.class);
    }
}

package com.telerikacademy.web.smartgarage.controllers.mvc;

import com.telerikacademy.web.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.models.Customer;
import com.telerikacademy.web.smartgarage.models.CustomerDto;
import com.telerikacademy.web.smartgarage.models.searchparameters.CustomerSearchParameters;
import com.telerikacademy.web.smartgarage.models.searchparameters.ModelSearchParameters;
import com.telerikacademy.web.smartgarage.services.contracts.CustomerService;
import com.telerikacademy.web.smartgarage.services.contracts.ModelService;
import com.telerikacademy.web.smartgarage.modelmapper.CustomerModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.validation.Valid;
import java.io.UnsupportedEncodingException;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
@RequestMapping("/customers")
public class CustomerMvcController {

    private final CustomerModelMapper customerModelMapper;
    private final CustomerService customerService;
    private final ModelService modelService;

    @Autowired
    public CustomerMvcController(CustomerModelMapper customerModelMapper,
                                 CustomerService customerService,
                                 ModelService modelService) {
        this.customerModelMapper = customerModelMapper;
        this.customerService = customerService;
        this.modelService = modelService;
    }

    @ModelAttribute("models")
    public List<com.telerikacademy.web.smartgarage.models.Model> models() {
        return modelService
                .getAll(new ModelSearchParameters())
                .stream()
                .sorted(Comparator.comparing(m -> m.getManufacturer().getName()))
                .collect(Collectors.toList());
    }

    @GetMapping
    @PreAuthorize("hasRole('EMPLOYEE')")
    public String showAllCustomers(Model model,
                                   @RequestParam("page") Optional<Integer> page,
                                   @RequestParam("size") Optional<Integer> size) {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);

        if (!model.containsAttribute("customerSearchParameters")) {
            model.addAttribute("customerSearchParameters", new CustomerSearchParameters());
        }

        Page<Customer> customerPage = customerService.findPaginated(PageRequest.of(currentPage - 1, pageSize)
                , (CustomerSearchParameters) model.getAttribute("customerSearchParameters"));
        List<Customer> customerList = customerPage.getContent();

        model.addAttribute("customerList", customerList);
        model.addAttribute("customerPage", customerPage);

        int totalPages = customerPage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }

        return "customers";
    }

    @GetMapping("/reset")
    @PreAuthorize("hasRole('EMPLOYEE')")
    public String handleCustomerReset(Model model) {
        return "redirect:/customers";
    }

    @PostMapping("/search")
    @PreAuthorize("hasRole('EMPLOYEE')")
    public String handleCustomerSearch(Model model, @ModelAttribute("customerSearchParameters") CustomerSearchParameters customerSearchParameters) {
        model.addAttribute("customerList", customerService.getAll(customerSearchParameters));
        return "customers";
    }

    @GetMapping("/create")
    @PreAuthorize("hasRole('EMPLOYEE')")
    public String showCreateCustomerPage(Model model) {
        model.addAttribute("customer", new CustomerDto());
        return "customer-new";
    }

    @PostMapping("/create")
    @PreAuthorize("hasRole('EMPLOYEE')")
    public String handleCreateVisitPage(Model model,
                                        @ModelAttribute("customer") @Valid CustomerDto customerDto,
                                        BindingResult errors) {
        try {
            Customer customer = customerModelMapper.fromDto(customerDto);
            customerService.create(customer);
            return "redirect:/customers";
        } catch (DuplicateEntityException e) {
            if (e.getMessage().contains("email")) {
                errors.rejectValue("user.userCredentials.email", "email_error", e.getMessage());
            } else {
                errors.rejectValue("user.phone", "phone_error", e.getMessage());
            }
            return "customer-new";
        } catch (UnsupportedEncodingException | MessagingException e1) {
            return "error";
        }
    }

    @GetMapping("/{id}/update")
    @PreAuthorize("hasRole('EMPLOYEE')")
    public String showEditProfilePage(@PathVariable Long id, Model model) {
        Customer customer;
        CustomerDto customerDto;
        try {
            customer = customerService.getById(id);
            customerDto = customerModelMapper.toDto(customer);
        } catch (EntityNotFoundException e) {
            return "redirect:/customers";
        }
        model.addAttribute("customer", customerDto);
        model.addAttribute("customerId", customer.getId());
        return "customer-update";
    }

    @PostMapping("/{id}/update")
    @PreAuthorize("hasRole('EMPLOYEE')")
    public String editCustomerProfile(@ModelAttribute("customer") CustomerDto customerDto,
                                      @Valid @PathVariable Long id,
                                      BindingResult errors) {
        try {
            Customer customer = customerModelMapper.fromDto(customerDto, id);
            customerService.update(customer);
        } catch (DuplicateEntityException e) {
            if (e.getMessage().contains("phone")) {
                errors.rejectValue("user.phone", "duplicate_phone", e.getMessage());
            } else {
                errors.rejectValue("user.UserCredentials.email", "duplicate_email", e.getMessage());
            }
            return "customer-update";
        } catch (EntityNotFoundException e) {
            errors.rejectValue("licensePlate", "license_plate", e.getMessage());
            return "customer-update";
        }
        return "redirect:/customers";
    }

    @GetMapping("/{id}/delete")
    @PreAuthorize("hasRole('EMPLOYEE')")
    public String deleteCustomer(@PathVariable Long id, Model model) {
        try {
            customerService.delete(id);
            return "redirect:/customers";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }
}

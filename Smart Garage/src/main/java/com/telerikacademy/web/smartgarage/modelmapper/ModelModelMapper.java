package com.telerikacademy.web.smartgarage.modelmapper;

import com.telerikacademy.web.smartgarage.models.Model;
import com.telerikacademy.web.smartgarage.models.ModelDto;
import com.telerikacademy.web.smartgarage.repositories.contracts.ManufacturerRepository;
import com.telerikacademy.web.smartgarage.repositories.contracts.ModelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ModelModelMapper {
    private final ModelRepository modelRepository;
    private final ManufacturerRepository manufacturerRepository;

    @Autowired
    public ModelModelMapper(ModelRepository modelRepository,
                            ManufacturerRepository manufacturerRepository) {
        this.modelRepository = modelRepository;
        this.manufacturerRepository = manufacturerRepository;
    }

    public Model fromDto(ModelDto modelDto) {
        Model model = new Model();
        dtoToObject(modelDto, model);
        return model;
    }

    public Model fromDto(ModelDto modelDto, Long id) {
        Model model = this.modelRepository.getById(id);
        dtoToObject(modelDto, model);
        return model;
    }

    public ModelDto toDto(Model model) {
        ModelDto modelDto = new ModelDto();
        modelDto.setName(model.getName());
        modelDto.setManufacturerId(model.getManufacturer().getId());
        return modelDto;
    }

    private void dtoToObject(ModelDto modelDto, Model model) {
        model.setName(modelDto.getName());
        model.setManufacturer(manufacturerRepository.getById(modelDto.getManufacturerId()));
    }
}

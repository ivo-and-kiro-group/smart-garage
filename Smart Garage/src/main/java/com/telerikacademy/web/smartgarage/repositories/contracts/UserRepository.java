package com.telerikacademy.web.smartgarage.repositories.contracts;

import com.telerikacademy.web.smartgarage.models.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository {

    User getById(Long id);

    User getByEmail(String email);

    User findByResetPassword(String email);

    User update(User user);

}

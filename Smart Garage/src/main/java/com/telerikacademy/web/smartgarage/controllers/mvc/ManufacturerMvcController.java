package com.telerikacademy.web.smartgarage.controllers.mvc;

import com.telerikacademy.web.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.modelmapper.ManufacturerModelMapper;
import com.telerikacademy.web.smartgarage.models.Manufacturer;
import com.telerikacademy.web.smartgarage.models.ManufacturerDto;
import com.telerikacademy.web.smartgarage.models.searchparameters.EmployeeSearchParameters;
import com.telerikacademy.web.smartgarage.models.searchparameters.ManufacturerSearchParameters;
import com.telerikacademy.web.smartgarage.services.contracts.ManufacturerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
@RequestMapping("/manufacturers")
public class ManufacturerMvcController {

    private final ManufacturerModelMapper manufacturerModelMapper;
    private final ManufacturerService manufacturerService;

    @Autowired
    public ManufacturerMvcController(ManufacturerModelMapper manufacturerModelMapper, ManufacturerService manufacturerService) {
        this.manufacturerModelMapper = manufacturerModelMapper;
        this.manufacturerService = manufacturerService;
    }

    @GetMapping
    @PreAuthorize("hasRole('EMPLOYEE')")
    public String showAllManufacturers(Model model,
                                   @RequestParam("page") Optional<Integer> page,
                                   @RequestParam("size") Optional<Integer> size) {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);


        model.addAttribute("manufacturerSearchParameters", new ManufacturerSearchParameters());


        Page<Manufacturer> manufacturerPage = manufacturerService.findPaginated(PageRequest.of(currentPage - 1, pageSize)
                , (ManufacturerSearchParameters) model.getAttribute("manufacturerSearchParameters"));
        List<Manufacturer> manufacturerList = manufacturerPage.getContent();

        model.addAttribute("manufacturerList", manufacturerList);
        model.addAttribute("manufacturerPage", manufacturerPage);

        int totalPages = manufacturerPage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }

        return "manufacturers";
    }

    @GetMapping("/reset")
    public String handleManufacturerReset(Model model) {
        return "redirect:/manufacturers";
    }

    @PostMapping("/search")
    @PreAuthorize("hasRole('EMPLOYEE')")
    public String handleManufacturerSearch(Model model, @ModelAttribute("manufacturerSearchParameters") ManufacturerSearchParameters manufacturerSearchParameters) {
        model.addAttribute("manufacturerList", manufacturerService.getAll(manufacturerSearchParameters));
        return "manufacturers";
    }

    @GetMapping("/create")
    @PreAuthorize("hasRole('EMPLOYEE')")
    public String showCreateManufacturerPage(Model model) {
        model.addAttribute("manufacturer", new ManufacturerDto());
        return "manufacturer-new";
    }

    @PostMapping("/create")
    @PreAuthorize("hasRole('EMPLOYEE')")
    public String handleCreateVisitPage(Model model,
                                        @Valid @ModelAttribute("manufacturer") ManufacturerDto manufacturerDto,
                                        BindingResult errors) {
        try {
            Manufacturer manufacturer = manufacturerModelMapper.fromDto(manufacturerDto);
            manufacturerService.create(manufacturer);
            return "redirect:/manufacturers";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("phone", "phone_error", e.getMessage());
            return "manufacturer-new";
        }
    }

    @GetMapping("/{id}/update")
    @PreAuthorize("hasRole('EMPLOYEE')")
    public String showEditProfilePage(@PathVariable Long id,Model model) {
        Manufacturer manufacturer;
        ManufacturerDto manufacturerDto;
        try {
            manufacturer = manufacturerService.getById(id);
            manufacturerDto = manufacturerModelMapper.toDto(manufacturer);
        }catch (EntityNotFoundException e){
            return "redirect:/manufacturers";
        }
        model.addAttribute("manufacturer", manufacturerDto);
        model.addAttribute("manufacturerId", manufacturer.getId());
        return "manufacturer-update";
    }

    @PostMapping("/{id}/update")
    @PreAuthorize("hasRole('EMPLOYEE')")
    public String editManufacturerProfile(@Valid @ModelAttribute("manufacturer") ManufacturerDto manufacturerDto,
                                      @PathVariable Long id,
                                      BindingResult errors) {
        try {
            Manufacturer manufacturer = manufacturerModelMapper.fromDto(manufacturerDto, id);
            manufacturerService.update(manufacturer);
        } catch (DuplicateEntityException e) {
            errors.rejectValue("name", "duplicate_name", e.getMessage());
            return "manufacturerProfile-edit";
        } catch (EntityNotFoundException e) {
            return "not-found";
        }
        return "redirect:/manufacturers";
    }

    @GetMapping("/{id}/delete")
    @PreAuthorize("hasRole('EMPLOYEE')")
    public String deleteManufacturer(@PathVariable Long id, Model model) {
        try {
            manufacturerService.delete(id);
            return "redirect:/manufacturers";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }
}

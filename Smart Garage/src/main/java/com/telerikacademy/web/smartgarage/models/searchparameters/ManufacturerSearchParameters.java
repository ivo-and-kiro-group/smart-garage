package com.telerikacademy.web.smartgarage.models.searchparameters;

public class ManufacturerSearchParameters {

    private String name;

    public ManufacturerSearchParameters(String name) {
        this.name = name;
    }

    public ManufacturerSearchParameters() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

package com.telerikacademy.web.smartgarage.services;

import com.telerikacademy.web.smartgarage.controllers.mvc.PDFController;
import com.telerikacademy.web.smartgarage.services.contracts.MailSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.Objects;

@Service
public class MailSenderImpl implements MailSender {

    private final JavaMailSender javaMailSender;

    @Autowired
    public MailSenderImpl(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @Override
    public void sendEmail(String email, String password) throws UnsupportedEncodingException, MessagingException {
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setFrom("autorepair.center.k.i@gmail.com", "AutoRepair Support");
        helper.setTo(email);

        String subject = "Registration";

        String content = "<p>Hello,</p>"
                + "<p>Your new account is created.</p>"
                + "<p>Click the link below to Login:</p>"
                + "<p>Your password is:" + password
                + "<p><b><a href=\"" + "http://localhost:8080/login" + "\">Login</a></b></p>";


        helper.setSubject(subject);
        helper.setText(content, true);

        javaMailSender.send(message);
    }

    @Override
    public void sendEmailWithAttachment(String toEmail,
                                        String body,
                                        String subject,
                                        String visitId) throws MessagingException {

        MimeMessage mimeMessage = javaMailSender.createMimeMessage();

        MimeMessageHelper mimeMessageHelper
                = new MimeMessageHelper(mimeMessage, true);

        mimeMessageHelper.setFrom("kiroandivokiroandivo@gmail.com");
        mimeMessageHelper.setTo(toEmail);
        mimeMessageHelper.setText(body);
        mimeMessageHelper.setSubject(subject);

        FileSystemResource fileSystem
                = new FileSystemResource(new File("C:\\Users\\ivayl\\Downloads\\invoice.pdf"));

        mimeMessageHelper.addAttachment(Objects.requireNonNull(fileSystem.getFilename()),
                fileSystem);

        javaMailSender.send(mimeMessage);
        System.out.println("Mail Send...");

        try
        {
            Files.deleteIfExists(Paths.get("C:\\Users\\ivayl\\Downloads\\invoice.pdf"));
        }
        catch(NoSuchFileException e)
        {
            System.out.println("No such file/directory exists");
        }
        catch(DirectoryNotEmptyException e)
        {
            System.out.println("Directory is not empty.");
        }
        catch(IOException e)
        {
            System.out.println("Invalid permissions.");
        }

        System.out.println("Deletion successful.");
    }

}

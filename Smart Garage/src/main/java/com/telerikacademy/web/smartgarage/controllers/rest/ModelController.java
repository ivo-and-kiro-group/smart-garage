package com.telerikacademy.web.smartgarage.controllers.rest;

import com.telerikacademy.web.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.models.Model;
import com.telerikacademy.web.smartgarage.models.ModelDto;
import com.telerikacademy.web.smartgarage.models.searchparameters.ModelSearchParameters;
import com.telerikacademy.web.smartgarage.services.contracts.ModelService;
import com.telerikacademy.web.smartgarage.modelmapper.ModelModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/models")
public class ModelController {
    private final ModelService service;
    private final ModelModelMapper modelMapper;


    @Autowired
    public ModelController(ModelService service, ModelModelMapper modelMapper) {
        this.service = service;
        this.modelMapper = modelMapper;
    }

    @GetMapping
    @PreAuthorize("hasRole('EMPLOYEE') or hasRole('CUSTOMER')")
    public List<Model> getAll(String name,
                              String manufacturerName) {
        return service.getAll(new ModelSearchParameters(name, manufacturerName));
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('EMPLOYEE') or hasRole('CUSTOMER')")
    public Model getById(@PathVariable Long id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    e.getMessage());
        }
    }

    @GetMapping("/name/{name}")
    @PreAuthorize("hasRole('EMPLOYEE') or hasRole('CUSTOMER')")
    public Model getByName(@PathVariable String name) {
        try {
            return service.getByName(name);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    e.getMessage());
        }
    }

    @PostMapping
    @PreAuthorize("hasRole('EMPLOYEE')")
    public Model create(@Valid @RequestBody ModelDto modelDto) {
        try {
            Model model = modelMapper.fromDto(modelDto);
            return service.create(model);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }

    }

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('EMPLOYEE')")
    public Model update(@PathVariable Long id, @Valid @RequestBody ModelDto modelDto) {
        try {
            Model model = modelMapper.fromDto(modelDto, id);
            return service.update(model);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('EMPLOYEE')")
    public void delete(@PathVariable Long id) {
        try {
            service.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}

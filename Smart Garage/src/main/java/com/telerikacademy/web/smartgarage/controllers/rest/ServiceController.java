package com.telerikacademy.web.smartgarage.controllers.rest;

import com.telerikacademy.web.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.models.Service;
import com.telerikacademy.web.smartgarage.models.ServiceDto;
import com.telerikacademy.web.smartgarage.models.searchparameters.ServiceSearchParameters;
import com.telerikacademy.web.smartgarage.services.contracts.ServiceService;
import com.telerikacademy.web.smartgarage.modelmapper.ServiceModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/services")
public class ServiceController {

    private final ServiceService service;
    private final ServiceModelMapper modelMapper;

    @Autowired
    public ServiceController(ServiceService service, ServiceModelMapper modelMapper) {
        this.service = service;
        this.modelMapper = modelMapper;
    }

    @GetMapping
    @PreAuthorize("hasRole('EMPLOYEE') or hasRole('CUSTOMER')")
    public List<Service> getAll(@RequestParam(required = false) String name,
                                @RequestParam(required = false) Double fromPrice,
                                @RequestParam(required = false) Double toPrice) {
        return service.getAll(new ServiceSearchParameters(name, fromPrice, toPrice));
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('EMPLOYEE') or hasRole('CUSTOMER')")
    public Service getById(@Valid @PathVariable Long id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    @PreAuthorize("hasRole('EMPLOYEE')")
    public Service create(@RequestBody @Valid ServiceDto serviceDto) {
        try {
            Service createService = modelMapper.fromDto(serviceDto);
            return service.create(createService);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('EMPLOYEE')")
    public Service update(@Valid @PathVariable Long id, @Valid @RequestBody ServiceDto serviceDto) {
        try {
            Service updateService = modelMapper.fromDto(serviceDto, id);
            return service.update(updateService);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('EMPLOYEE')")
    public void delete(@Valid @PathVariable Long id) {
        try {
            service.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}

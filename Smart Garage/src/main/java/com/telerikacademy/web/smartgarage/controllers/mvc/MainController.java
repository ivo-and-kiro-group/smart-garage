package com.telerikacademy.web.smartgarage.controllers.mvc;

import com.telerikacademy.web.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.web.smartgarage.modelmapper.CustomerModelMapper;
import com.telerikacademy.web.smartgarage.modelmapper.EmployeeModelMapper;
import com.telerikacademy.web.smartgarage.models.*;
import com.telerikacademy.web.smartgarage.models.searchparameters.VisitSearchParameters;
import com.telerikacademy.web.smartgarage.repositories.contracts.UserRepository;
import com.telerikacademy.web.smartgarage.services.contracts.CustomerService;
import com.telerikacademy.web.smartgarage.services.contracts.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class MainController {

    @GetMapping("/")
    public String root() {
        return "index";
    }

    @GetMapping("/login")
    public String login(Model model) {
        return "login";
    }

    @GetMapping("/activities")
    @PreAuthorize("hasRole('EMPLOYEE')")
    public String activities(Model model) {
        return "activities";
    }


    @GetMapping("/contact")
    public String showContactPage() {
        return "contact";
    }

}

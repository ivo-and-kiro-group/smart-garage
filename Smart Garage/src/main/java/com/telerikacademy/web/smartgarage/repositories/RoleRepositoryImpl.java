package com.telerikacademy.web.smartgarage.repositories;

import com.telerikacademy.web.smartgarage.models.Role;
import com.telerikacademy.web.smartgarage.repositories.contracts.RoleRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class RoleRepositoryImpl extends AbstractGenericGetRepository<Role> implements RoleRepository {

    @Autowired
    public RoleRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public Role getById(Long id) {
        return super.getByField("id", id, Role.class);
    }

    @Override
    public Role getByName(String name) {
        return super.getByField("name", name, Role.class);
    }
}

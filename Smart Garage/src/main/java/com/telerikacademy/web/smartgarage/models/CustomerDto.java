package com.telerikacademy.web.smartgarage.models;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Set;

public class CustomerDto {

    @Valid
    @NotNull(message = "User should not be null.")
    private UserDto user;

    public CustomerDto() {
    }

    public UserDto getUser() {
        return user;
    }

    public void setUser(UserDto user) {
        this.user = user;
    }
}

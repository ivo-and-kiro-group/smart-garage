package com.telerikacademy.web.smartgarage.controllers.mvc;

import com.telerikacademy.web.smartgarage.Utility;
import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.models.Customer;
import com.telerikacademy.web.smartgarage.models.User;
import com.telerikacademy.web.smartgarage.services.contracts.CustomerService;
import com.telerikacademy.web.smartgarage.services.contracts.UserService;
import net.bytebuddy.utility.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;

@Controller
public class ForgotPasswordController {

    private final UserService userService;
    private final JavaMailSender sender;

    @Autowired
    public ForgotPasswordController(UserService userService, JavaMailSender sender) {
        this.userService = userService;
        this.sender = sender;
    }

    @GetMapping("/forgot_password")
    @PreAuthorize("!isAuthenticated()")
    public String showForgotPasswordForm(Model model) {
        model.addAttribute("pageTitle", "Forgot Password");
        return "forgot_password_form";
    }

    @PostMapping("/forgot_password")
    public String processForgotPasswordForm(HttpServletRequest request, Model model) {

        String email = request.getParameter("email");

        String token = RandomString.make(45);

        try {
            userService.updateResetPasswordToken(token, email);

            String resetPasswordLink = Utility.getSiteURL(request) + "/reset_password?token=" + token;

            sendEmail(email, resetPasswordLink);

            model.addAttribute("message",
                    "We have sent a reset password link to email. Please check.");
        } catch (EntityNotFoundException ex) {
            model.addAttribute("error", ex.getMessage());
        } catch (UnsupportedEncodingException | MessagingException e) {
            model.addAttribute("error", "Error while sending email.");
        }

        return "forgot_password_form";
    }

    @GetMapping("/reset_password")
    @PreAuthorize("!isAuthenticated()")
    public String showResetPasswordForm(@Param(value = "token") String token,
                                        Model model) {

        try {
            User user = userService.getUserByResetPasswordToken(token);
        } catch (EntityNotFoundException e) {
            model.addAttribute("message", "Invalid Token");
            return "message";
        }

        model.addAttribute("token", token);
        return "reset_password";
    }

    @PostMapping("/reset_password")
    public String processResetPassword(HttpServletRequest request, Model model) {
        String token = request.getParameter("token");
        String password = request.getParameter("password1");

        try {
            User user = userService.getUserByResetPasswordToken(token);
            userService.updatePassword(user, password);
            model.addAttribute("success", "You have successfully changed your password.");
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", "Invalid Token");
            return "error";
        }

        return "reset_password";
    }

    private void sendEmail(String email, String resetPasswordLink) throws UnsupportedEncodingException, MessagingException {
        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setFrom("autorepair.center.k.i@gmail.com", "AutoRepair Support");
        helper.setTo(email);

        String subject = "Here is the link to reset your password";

        String content = "<p>Hello,</p>"
                + "<p>You have requested to reset your password.</p>"
                + "<p>Click the link below to change your password:</p>"
                + "<p><b><a href=\"" + resetPasswordLink + "\">Change my password</a></b></p>"
                + "<p>Ignore this email if you do remember your password, or you have not made the request.</p>";

        helper.setSubject(subject);
        helper.setText(content, true);

        sender.send(message);
    }
}

package com.telerikacademy.web.smartgarage.services.contracts;

import com.telerikacademy.web.smartgarage.models.User;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

    void updateResetPasswordToken(String token, String email);

    User getUserByResetPasswordToken(String resetPasswordToken);

    void updatePassword(User user, String newPassword);
}

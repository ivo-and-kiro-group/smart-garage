package com.telerikacademy.web.smartgarage.repositories.contracts;

import com.telerikacademy.web.smartgarage.models.Vehicle;
import com.telerikacademy.web.smartgarage.models.searchparameters.VehicleSearchParameters;

import java.util.List;

public interface VehicleRepository {

    List<Vehicle> getAll(VehicleSearchParameters vsp);

    Vehicle getById(Long id);

    Vehicle getByLicensePlate(String licensePlate);

    Vehicle getByVin(String vin);

    Vehicle create(Vehicle vehicle);

    Vehicle update(Vehicle vehicle);

    void delete(Long id);

    boolean isVehicleExisting(String fieldValue,String fieldName, Long id);
}

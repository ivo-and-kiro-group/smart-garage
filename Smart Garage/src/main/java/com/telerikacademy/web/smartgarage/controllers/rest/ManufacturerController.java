package com.telerikacademy.web.smartgarage.controllers.rest;

import com.telerikacademy.web.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.models.Manufacturer;
import com.telerikacademy.web.smartgarage.models.ManufacturerDto;
import com.telerikacademy.web.smartgarage.models.searchparameters.ManufacturerSearchParameters;
import com.telerikacademy.web.smartgarage.services.contracts.ManufacturerService;
import com.telerikacademy.web.smartgarage.modelmapper.ManufacturerModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/manufacturers")
public class ManufacturerController {

    private final ManufacturerService service;
    private final ManufacturerModelMapper modelMapper;


    @Autowired
    public ManufacturerController(ManufacturerService service, ManufacturerModelMapper modelMapper) {
        this.service = service;
        this.modelMapper = modelMapper;
    }

    @GetMapping
    @PreAuthorize("hasRole('EMPLOYEE') or hasRole('CUSTOMER')")
    public List<Manufacturer> getAll(String name) {
        return service.getAll(new ManufacturerSearchParameters(name));
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('EMPLOYEE') or hasRole('CUSTOMER')")
    public Manufacturer getById(@PathVariable Long id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    e.getMessage());
        }
    }

    @GetMapping("/name/{name}")
    @PreAuthorize("hasRole('EMPLOYEE') or hasRole('CUSTOMER')")
    public Manufacturer getByName(@PathVariable String name) {
        try {
            return service.getByName(name);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    e.getMessage());
        }
    }

    @PostMapping
    @PreAuthorize("hasRole('EMPLOYEE')")
    public Manufacturer create(@Valid @RequestBody ManufacturerDto manufacturerDto) {
        try {
            Manufacturer manufacturer = modelMapper.fromDto(manufacturerDto);
            return service.create(manufacturer);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }

    }

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('EMPLOYEE')")
    public Manufacturer update(@PathVariable Long id, @Valid @RequestBody ManufacturerDto manufacturerDto) {
        try {
            Manufacturer manufacturer = modelMapper.fromDto(manufacturerDto, id);
            return service.update(manufacturer);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('EMPLOYEE')")
    public void delete(@PathVariable Long id) {
        try {
            service.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}

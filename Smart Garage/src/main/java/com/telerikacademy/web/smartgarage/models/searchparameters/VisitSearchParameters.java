package com.telerikacademy.web.smartgarage.models.searchparameters;

import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

public class VisitSearchParameters {

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate date;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate untilDate;
    private String licensePlate;
    private String customerEmail;

    public VisitSearchParameters() {
    }

    public VisitSearchParameters(LocalDate date,
                                 LocalDate untilDate,
                                 String licensePlate,
                                 String customerEmail) {
        this.date = date;
        this.untilDate = untilDate;
        this.licensePlate = licensePlate;
        this.customerEmail = customerEmail;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalDate getUntilDate() {
        return untilDate;
    }

    public void setUntilDate(LocalDate untilDate) {
        this.untilDate = untilDate;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

}

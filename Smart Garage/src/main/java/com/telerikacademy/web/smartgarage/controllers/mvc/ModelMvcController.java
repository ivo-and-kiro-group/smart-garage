package com.telerikacademy.web.smartgarage.controllers.mvc;

import com.telerikacademy.web.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.exceptions.LocalDateException;
import com.telerikacademy.web.smartgarage.modelmapper.ModelModelMapper;
import com.telerikacademy.web.smartgarage.models.Manufacturer;
import com.telerikacademy.web.smartgarage.models.ModelDto;
import com.telerikacademy.web.smartgarage.models.searchparameters.ManufacturerSearchParameters;
import com.telerikacademy.web.smartgarage.models.searchparameters.ModelSearchParameters;
import com.telerikacademy.web.smartgarage.services.contracts.ManufacturerService;
import com.telerikacademy.web.smartgarage.services.contracts.ModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
@RequestMapping("/models")
public class ModelMvcController {

    private final ModelService modelService;
    private final ModelModelMapper modelMapper;
    private final ManufacturerService manufacturerService;

    @Autowired
    public ModelMvcController(ModelService modelService,
                              ModelModelMapper modelMapper,
                              ManufacturerService manufacturerService) {
        this.modelService = modelService;
        this.modelMapper = modelMapper;
        this.manufacturerService = manufacturerService;
    }

    @ModelAttribute("manufactures")
    public List<Manufacturer> manufacturers() {
        return manufacturerService.getAll(new ManufacturerSearchParameters());
    }

    @GetMapping
    public String showAllServices(Model model,
                                  @RequestParam("page") Optional<Integer> page,
                                  @RequestParam("size") Optional<Integer> size) {

        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);

        Page<com.telerikacademy.web.smartgarage.models.Model> modelPage =
                modelService.findPaginated(PageRequest.of(currentPage - 1, pageSize));
        List<com.telerikacademy.web.smartgarage.models.Model> modelList = modelPage.getContent();

        model.addAttribute("modelList", modelList);
        model.addAttribute("modelSearchParameters", new ModelSearchParameters());
        model.addAttribute("modelPage", modelPage);

        int totalPages = modelPage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }

        return "models";
    }

    @GetMapping("/create")
    @PreAuthorize("hasRole('EMPLOYEE')")
    public String showCreateModel(Model model) {
        model.addAttribute("model", new ModelDto());
        return "model-new";
    }

    @PostMapping("/create")
    public String handleCreateModelPage(@Valid @ModelAttribute("model") ModelDto modelDto,
                                        BindingResult errors) {

        if (errors.hasErrors()) {
            return "model-new";
        }

        try {
            com.telerikacademy.web.smartgarage.models.Model model = modelMapper.fromDto(modelDto);
            modelService.create(model);
            return "redirect:/models";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("name", "duplicated_name", e.getMessage());
            return "model-new";
        }
    }

    @GetMapping("/{id}/update")
    @PreAuthorize("hasRole('EMPLOYEE')")
    public String showEditModel(@PathVariable Long id, Model model) {
        try {
            com.telerikacademy.web.smartgarage.models.Model modelToUpdate = modelService.getById(id);
            ModelDto modelDto = modelMapper.toDto(modelToUpdate);
            model.addAttribute("model", modelDto);
            return "model-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @PostMapping("/{id}/update")
    public String editModel(@PathVariable Long id,
                            @Valid @ModelAttribute("model") ModelDto modelDto,
                            BindingResult errors) {

        if (errors.hasErrors()) {
            return "model-update";
        }

        try {
            com.telerikacademy.web.smartgarage.models.Model modelToUpdate = modelMapper.fromDto(modelDto, id);
            modelService.update(modelToUpdate);
            return "redirect:/models";
        } catch (LocalDateException e) {
            errors.rejectValue("name", "Invalid_name", e.getMessage());
            return "model-update";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("name", "duplicated_name", e.getMessage());
            return "model-update";
        }
    }

    @GetMapping("/search")
    public String handleSearchGetVehicles() {
        return "redirect:/models";
    }

    @PostMapping("/search")
    public String handleVehiclesSearch(Model model,
                                       @ModelAttribute("modelSearchParameters")
                                               ModelSearchParameters modelSearchParameters) {

        model.addAttribute("modelList", modelService.getAll(modelSearchParameters));

        return "models";
    }

    @GetMapping("/{id}/delete")
    @PreAuthorize("hasRole('EMPLOYEE')")
    public String deleteModel(@PathVariable Long id, Model model) {
        try {
            modelService.delete(id);
            return "redirect:/models";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }
}

package com.telerikacademy.web.smartgarage.services.contracts;


import com.telerikacademy.web.smartgarage.models.Employee;
import com.telerikacademy.web.smartgarage.models.searchparameters.EmployeeSearchParameters;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface EmployeeService {

    List<Employee> getAll(EmployeeSearchParameters employeeSearchParameters);

    Employee getById(Long id);

    Employee getByEmail(String email);

    Employee getByPhone(String phone);

    Employee create(Employee employee);

    Employee update(Employee employee);

    void delete(Long id);

    Page<Employee> findPaginated(Pageable pageable,EmployeeSearchParameters employeeSearchParameters);
}

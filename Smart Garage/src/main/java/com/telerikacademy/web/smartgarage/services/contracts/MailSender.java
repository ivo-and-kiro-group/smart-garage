package com.telerikacademy.web.smartgarage.services.contracts;

import javax.mail.MessagingException;
import java.io.UnsupportedEncodingException;

public interface MailSender {
    void sendEmail(String email, String password) throws UnsupportedEncodingException, MessagingException;

    void sendEmailWithAttachment(String toEmail,
                                 String body,
                                 String subject,
                                 String visitId) throws MessagingException;

}

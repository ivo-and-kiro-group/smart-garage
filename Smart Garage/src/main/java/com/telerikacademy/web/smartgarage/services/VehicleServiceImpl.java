package com.telerikacademy.web.smartgarage.services;

import com.telerikacademy.web.smartgarage.exceptions.DeletingEntityException;
import com.telerikacademy.web.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.web.smartgarage.models.Vehicle;
import com.telerikacademy.web.smartgarage.models.Visit;
import com.telerikacademy.web.smartgarage.models.searchparameters.VehicleSearchParameters;
import com.telerikacademy.web.smartgarage.models.searchparameters.VisitSearchParameters;
import com.telerikacademy.web.smartgarage.repositories.AbstractGenericCrudRepository;
import com.telerikacademy.web.smartgarage.repositories.contracts.VehicleRepository;
import com.telerikacademy.web.smartgarage.services.contracts.VehicleService;
import com.telerikacademy.web.smartgarage.services.contracts.VisitService;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class VehicleServiceImpl extends AbstractGenericCrudRepository<Vehicle> implements VehicleService {

    private final VehicleRepository repository;
    private final VisitService visitService;

    @Autowired
    public VehicleServiceImpl(SessionFactory sessionFactory,
                              VehicleRepository repository, VisitService visitService) {
        super(sessionFactory);
        this.repository = repository;
        this.visitService = visitService;
    }

    public List<Vehicle> getAll(VehicleSearchParameters vsp) {
        return repository.getAll(vsp);
    }

    @Override
    public Vehicle getById(Long id) {
        return repository.getById(id);
    }

    @Override
    public Vehicle getByLicensePlate(String licensePlate) {
        return repository.getByLicensePlate(licensePlate);
    }

    @Override
    public Vehicle getByVin(String vin) {
        return repository.getByVin(vin);
    }

    @Override
    public Vehicle create(Vehicle vehicle) {
        validateDuplicate(vehicle, -1L);
        return repository.create(vehicle);
    }

    @Override
    public Vehicle update(Vehicle vehicle) {
        validateDuplicate(vehicle, vehicle.getId());
        return repository.update(vehicle);
    }

    @Override
    public void delete(Long id) {
        checkForExistingVisits(id);
        repository.delete(id);
    }

    @Override
    public Page<Vehicle> findPaginated(Pageable pageable) {

        List<Vehicle> allVehicles = getAll(new VehicleSearchParameters());

        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<Vehicle> list;

        if (allVehicles.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, allVehicles.size());
            list = allVehicles.subList(startItem, toIndex);
        }

        return new PageImpl<Vehicle>(list, PageRequest.of(currentPage, pageSize), allVehicles.size());
    }


    private void validateDuplicate(Vehicle vehicle, Long id) {
        if (repository.isVehicleExisting(vehicle.getLicensePlate(), "licensePlate", id)) {
            throw new DuplicateEntityException("Vehicle", "license plate", vehicle.getLicensePlate());
        }

        if (repository.isVehicleExisting(vehicle.getVin(), "vin", id)) {
            throw new DuplicateEntityException("Vehicle", "vin", vehicle.getVin());
        }
    }

    private void checkForExistingVisits(Long id) {
        Vehicle vehicle = getById(id);
        VisitSearchParameters vsp = new VisitSearchParameters();
        vsp.setLicensePlate(vehicle.getLicensePlate());
        List<Visit> visits = visitService.getAll(vsp);
        if (visits.size() != 0) {
            throw new DeletingEntityException("First You have to delete the visits linked to this vehicle.");
        }
    }
}

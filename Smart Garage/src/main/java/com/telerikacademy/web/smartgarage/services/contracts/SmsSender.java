package com.telerikacademy.web.smartgarage.services.contracts;

import com.telerikacademy.web.smartgarage.models.SmsRequest;

public interface SmsSender {

    void sendSms(SmsRequest smsRequest);

    // or maybe void sendSms(String phoneNumber, String message);
}

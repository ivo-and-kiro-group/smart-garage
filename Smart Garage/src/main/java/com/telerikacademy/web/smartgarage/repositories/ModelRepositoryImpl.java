package com.telerikacademy.web.smartgarage.repositories;

import com.telerikacademy.web.smartgarage.models.Model;
import com.telerikacademy.web.smartgarage.models.searchparameters.ModelSearchParameters;
import com.telerikacademy.web.smartgarage.repositories.contracts.ModelRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ModelRepositoryImpl extends AbstractGenericCrudRepository<Model> implements ModelRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public ModelRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Model> getAll(ModelSearchParameters modelSearchParameters) {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from Model where name like concat('%', :name,'%') " +
                    " and manufacturer.name like concat('%', :manufacturerName,'%') ", Model.class)
                    .setParameter("name", modelSearchParameters.getName() == null ?
                            "" : modelSearchParameters.getName())
                    .setParameter("manufacturerName", modelSearchParameters.getManufacturerName() == null ?
                            "" : modelSearchParameters.getManufacturerName())
                    .list();
        }
    }

    @Override
    public Model getById(Long id) {
        return super.getByField("id", id, Model.class);
    }

    @Override
    public Model getByName(String name) {
        return super.getByField("name", name, Model.class);
    }

    @Override
    public Model create(Model model) {
        return super.create(model);
    }

    @Override
    public Model update(Model model) {
        return super.update(model);
    }

    @Override
    public void delete(Long id) {
        super.delete(id, Model.class);
    }
}

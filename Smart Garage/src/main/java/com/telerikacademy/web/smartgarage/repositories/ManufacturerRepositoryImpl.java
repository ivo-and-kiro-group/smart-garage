package com.telerikacademy.web.smartgarage.repositories;

import com.telerikacademy.web.smartgarage.models.Manufacturer;
import com.telerikacademy.web.smartgarage.models.searchparameters.ManufacturerSearchParameters;
import com.telerikacademy.web.smartgarage.repositories.contracts.ManufacturerRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ManufacturerRepositoryImpl extends
        AbstractGenericCrudRepository<Manufacturer> implements ManufacturerRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public ManufacturerRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Manufacturer> getAll(ManufacturerSearchParameters manufacturerSearchParameters) {
        try (Session session = sessionFactory.openSession()) {
            Query<Manufacturer> query = session.createQuery("from Manufacturer where name " +
                    "like concat('%', :name,'%') ", Manufacturer.class);
            query.setParameter("name", manufacturerSearchParameters.getName() == null ? "" : manufacturerSearchParameters.getName());
            return query.list();

        }
    }

    @Override
    public Manufacturer getById(Long id) {
        return super.getByField("id", id, Manufacturer.class);
    }

    @Override
    public Manufacturer getByName(String name) {
        return super.getByField("name", name, Manufacturer.class);
    }

    @Override
    public Manufacturer create(Manufacturer manufacturer) {
        return super.create(manufacturer);
    }

    @Override
    public Manufacturer update(Manufacturer manufacturer) {
        return super.update(manufacturer);
    }

    @Override
    public void delete(Long id) {
        super.delete(id, Manufacturer.class);
    }
}

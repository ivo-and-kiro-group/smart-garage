package com.telerikacademy.web.smartgarage.services;

import com.telerikacademy.web.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.models.Manufacturer;
import com.telerikacademy.web.smartgarage.models.searchparameters.ManufacturerSearchParameters;
import com.telerikacademy.web.smartgarage.repositories.contracts.ManufacturerRepository;
import com.telerikacademy.web.smartgarage.services.contracts.ManufacturerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class ManufacturerServiceImpl implements ManufacturerService {
    private final ManufacturerRepository manufacturerRepository;

    @Autowired
    public ManufacturerServiceImpl(ManufacturerRepository manufacturerRepository) {
        this.manufacturerRepository = manufacturerRepository;
    }

    @Override
    public List<Manufacturer> getAll(ManufacturerSearchParameters manufacturerSearchParameters) {
        return manufacturerRepository.getAll(manufacturerSearchParameters);
    }

    @Override
    public Manufacturer getById(Long id) {
        return manufacturerRepository.getById(id);
    }

    @Override
    public Manufacturer getByName(String name) {
        return manufacturerRepository.getByName(name);
    }

    @Override
    public Manufacturer create(Manufacturer manufacturer) {
        validateDuplicate(manufacturer);
        return manufacturerRepository.create(manufacturer);
    }

    @Override
    public Manufacturer update(Manufacturer manufacturer) {
        validateDuplicateUpdate(manufacturer);
        return manufacturerRepository.update(manufacturer);
    }

    @Override
    public void delete(Long id) {
        manufacturerRepository.delete(id);
    }

    @Override
    public Page<Manufacturer> findPaginated(Pageable pageable, ManufacturerSearchParameters manufacturerSearchParameters) {
        List<Manufacturer> allManufacturers = getAll(manufacturerSearchParameters);

        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<Manufacturer> list;

        if (allManufacturers.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, allManufacturers.size());
            list = allManufacturers.subList(startItem, toIndex);
        }

        return new PageImpl<Manufacturer>(list, PageRequest.of(currentPage, pageSize), allManufacturers.size());
    }

    private void validateDuplicate(Manufacturer manufacturer) {
        boolean duplicateNameExists = true;

        try {
            manufacturerRepository.getByName(manufacturer.getName());
        } catch (EntityNotFoundException e) {
            duplicateNameExists = false;
        }

        if (duplicateNameExists) {
            throw new DuplicateEntityException("Manufacturer", "name", manufacturer.getName());
        }
    }

    private void validateDuplicateUpdate(Manufacturer manufacturer) {
        boolean duplicateNameExists = true;

        try {
            Manufacturer manufacturer1 = manufacturerRepository.getByName(manufacturer.getName());
            if (manufacturer1.getId().equals(manufacturer.getId())) {
                duplicateNameExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateNameExists = false;
        }

        if (duplicateNameExists) {
            throw new DuplicateEntityException("Manufacturer", "name", manufacturer.getName());
        }
    }
}

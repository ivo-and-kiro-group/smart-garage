package com.telerikacademy.web.smartgarage.models.searchparameters;

public class VehicleSearchParameters {

    private String customerEmail;
    private String licensePlate;
    private String vin;
    private String phone;

    public VehicleSearchParameters(String customerEmail, String licensePlate, String vin, String phone) {
        this.customerEmail = customerEmail;
        this.licensePlate = licensePlate;
        this.vin = vin;
        this.phone = phone;
    }

    public VehicleSearchParameters() {
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}

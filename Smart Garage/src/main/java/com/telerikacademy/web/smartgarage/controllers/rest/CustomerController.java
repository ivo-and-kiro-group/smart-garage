package com.telerikacademy.web.smartgarage.controllers.rest;

import com.telerikacademy.web.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.models.Customer;
import com.telerikacademy.web.smartgarage.models.CustomerDto;
import com.telerikacademy.web.smartgarage.models.Vehicle;
import com.telerikacademy.web.smartgarage.models.searchparameters.CustomerSearchParameters;
import com.telerikacademy.web.smartgarage.services.contracts.CustomerService;
import com.telerikacademy.web.smartgarage.modelmapper.CustomerModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.mail.MessagingException;
import javax.validation.Valid;
import java.io.UnsupportedEncodingException;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("api/customers")
public class CustomerController {
    private final CustomerService service;
    private final CustomerModelMapper modelMapper;

    @Autowired
    public CustomerController(CustomerService service, CustomerModelMapper modelMapper) {
        this.service = service;
        this.modelMapper = modelMapper;
    }

    @GetMapping
    @PreAuthorize("hasRole('EMPLOYEE')")
    public List<Customer> getAll(@RequestParam(required = false) String email,
                                 @RequestParam(required = false) String firstName,
                                 @RequestParam(required = false) String lastName,
                                 @RequestParam(required = false) String phone,
                                 @RequestParam(required = false) String orderBy,
                                 @RequestParam(required = false) Long modelId,
                                 @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fromDate,
                                 @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate untilDate) {
        return service.getAll(new CustomerSearchParameters(email,
                firstName,
                lastName,
                phone,
                modelId,
                fromDate,
                untilDate,
                orderBy));
    }

    @GetMapping("/numberOfCustomers")
    public Integer getNumberOfCustomers() {
        return service.getAll(new CustomerSearchParameters()).size();
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('CUSTOMER') or hasRole('EMPLOYEE')")
    public Customer getById(@PathVariable Long id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    e.getMessage());
        }
    }

    @GetMapping("/email/{email}")
    @PreAuthorize("hasRole('EMPLOYEE')")
    public Customer getByEmail(@PathVariable String email) {
        try {
            return service.getByEmail(email);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    e.getMessage());
        }
    }

    @GetMapping("/phone/{phone}")
    @PreAuthorize("hasRole('EMPLOYEE')")
    public Customer getByUsername(@PathVariable String phone) {
        try {
            return service.getByPhone(phone);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    e.getMessage());
        }
    }

    @PostMapping
    @PreAuthorize("hasRole('EMPLOYEE') or hasRole('ADMIN')")
    public Customer create(@Valid @RequestBody CustomerDto customerDto) {
        try {
            Customer customer = modelMapper.fromDto(customerDto);
            return service.create(customer);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (MessagingException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnsupportedEncodingException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }

    }

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('CUSTOMER') or hasRole('EMPLOYEE')")
    public Customer update(@PathVariable Long id, @Valid @RequestBody CustomerDto customerDto) {
        try {
            Customer customer = modelMapper.fromDto(customerDto, id);
            return service.update(customer);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('EMPLOYEE') or hasRole('CUSTOMER') or hasRole('ADMIN')")
    public void delete(@PathVariable Long id) {
        try {
            service.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}

package com.telerikacademy.web.smartgarage.controllers.mvc;

import com.telerikacademy.web.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.modelmapper.CustomerModelMapper;
import com.telerikacademy.web.smartgarage.modelmapper.EmployeeModelMapper;
import com.telerikacademy.web.smartgarage.modelmapper.UserModelMapper;
import com.telerikacademy.web.smartgarage.models.*;
import com.telerikacademy.web.smartgarage.models.searchparameters.VisitSearchParameters;
import com.telerikacademy.web.smartgarage.repositories.contracts.UserRepository;
import com.telerikacademy.web.smartgarage.services.contracts.CustomerService;
import com.telerikacademy.web.smartgarage.services.contracts.EmployeeService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/profile")
public class ProfileMvcController {

    private final UserRepository userRepository;
    private final CustomerService customerService;
    private final EmployeeService employeeService;
    private final UserModelMapper userModelMapper;
    private final CustomerModelMapper customerModelMapper;
    private final EmployeeModelMapper employeeModelMapper;

    public ProfileMvcController(UserRepository userRepository,
                                CustomerService customerService,
                                EmployeeService employeeService,
                                UserModelMapper userModelMapper,
                                CustomerModelMapper customerModelMapper,
                                EmployeeModelMapper employeeModelMapper) {
        this.userRepository = userRepository;
        this.customerService = customerService;
        this.employeeService = employeeService;
        this.userModelMapper = userModelMapper;
        this.customerModelMapper = customerModelMapper;
        this.employeeModelMapper = employeeModelMapper;
    }

    @GetMapping
    @PreAuthorize("isAuthenticated()")
    public String profile(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        User user = userRepository.getByEmail(currentPrincipalName);
        model.addAttribute("currentUser", user);
        model.addAttribute("visitSearchParameters", new VisitSearchParameters());
        return "profile";
    }

    @GetMapping("/update")
    @PreAuthorize("isAuthenticated()")
    public String showEditProfilePage(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        Customer customer = customerService.getByEmail(currentPrincipalName);
        CustomerDto customerDto = customerModelMapper.toDto(customer);
        model.addAttribute("customer", customerDto);
        return "customer-profile-update";
    }

    @PostMapping("/update")
    public String handleEditProfile(@Valid @ModelAttribute("customer") CustomerDto customerDto,
                                    BindingResult errors) {
        if (errors.hasErrors()) {
            return "customer-profile-update";
        }
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();

        try {
            Customer customer = customerService.getByEmail(currentPrincipalName);
            Customer customerToUpdate = customerModelMapper.fromDto(customerDto, customer.getId());
            customerService.update(customerToUpdate);
        } catch (EntityNotFoundException | DuplicateEntityException e) {
            if (e.getMessage().contains("email")) {
                errors.rejectValue("email", "invalid_email", e.getMessage());
                return "customer-profile-update";
            } else if (e.getMessage().contains("phone")) {
                errors.rejectValue("phone", "invalid_phone", e.getMessage());
                return "customer-profile-update";
            }
        }
        return "redirect:/profile";
    }
}

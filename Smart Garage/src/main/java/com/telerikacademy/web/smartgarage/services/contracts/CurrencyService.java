package com.telerikacademy.web.smartgarage.services.contracts;

public interface CurrencyService {

    Double rateConvert(String from,String to);

}

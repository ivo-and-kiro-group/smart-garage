package com.telerikacademy.web.smartgarage.services.contracts;

import com.telerikacademy.web.smartgarage.models.Employee;
import com.telerikacademy.web.smartgarage.models.Model;
import com.telerikacademy.web.smartgarage.models.searchparameters.ModelSearchParameters;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ModelService {
    List<Model> getAll(ModelSearchParameters modelSearchParameters);

    Model getById(Long id);

    Model getByName(String name);

    Model create(Model model);

    Model update(Model model);

    void delete(Long id);

    Page<Model> findPaginated(Pageable pageable);
}

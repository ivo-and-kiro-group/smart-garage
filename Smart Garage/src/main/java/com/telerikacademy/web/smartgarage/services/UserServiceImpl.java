package com.telerikacademy.web.smartgarage.services;


import com.telerikacademy.web.smartgarage.config.SecurityConfig;
import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.models.Role;
import com.telerikacademy.web.smartgarage.models.User;
import com.telerikacademy.web.smartgarage.repositories.contracts.UserRepository;
import com.telerikacademy.web.smartgarage.services.contracts.UserService;
import org.hibernate.internal.build.AllowPrintStacktrace;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user;
        try {
            user = userRepository.getByEmail(email);
        } catch (EntityNotFoundException entityNotFoundException) {
            throw new UsernameNotFoundException("Invalid username or password.");
        }

        return new org.springframework.security.core.userdetails.User(user.getUserCredentials().getEmail(),
                user.getUserCredentials().getPassword(),
                mapRolesToAuthorities(user.getRoles()));
    }

    private Collection<? extends GrantedAuthority> mapRolesToAuthorities(Collection<Role> roles) {
        return roles.stream()
                .map(role -> new SimpleGrantedAuthority(role.getName()))
                .collect(Collectors.toList());
    }

    @Override
    public void updateResetPasswordToken(String token, String email) {
        try {
            User user = userRepository.getByEmail(email);
            user.getUserCredentials().setResetPassword(token);
            userRepository.update(user);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("Customer", "email", email);
        }
    }

    @Override
    public User getUserByResetPasswordToken(String resetPasswordToken) {
        return userRepository.findByResetPassword(resetPasswordToken);
    }

    @Override
    public void updatePassword(User user, String newPassword) {
        String encodePassword = SecurityConfig.passwordEncoder().encode(newPassword);

        user.getUserCredentials().setPassword(encodePassword);
        user.getUserCredentials().setResetPassword(null);

        userRepository.update(user);
    }
}

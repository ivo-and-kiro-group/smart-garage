package com.telerikacademy.web.smartgarage.repositories;

import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.models.Customer;
import com.telerikacademy.web.smartgarage.models.Employee;
import com.telerikacademy.web.smartgarage.models.searchparameters.EmployeeSearchParameters;
import com.telerikacademy.web.smartgarage.repositories.contracts.EmployeeRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class EmployeeRepositoryImpl extends AbstractGenericCrudRepository<Employee> implements EmployeeRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public EmployeeRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Employee> getAll(EmployeeSearchParameters employeeSearchParameters) {

        try (Session session = sessionFactory.openSession()) {
            Query<Employee> query = session.createQuery(" from Employee as e " +
                    " where e.user.userCredentials.email like concat('%', :email,'%') " +
                    " and e.user.firstName like concat('%', :firstName,'%') " +
                    " and e.user.lastName like concat('%', :lastName,'%') " +
                    " and e.user.phone like concat('%', :phone,'%') ", Employee.class);
            query.setParameter("email", employeeSearchParameters.getEmail() == null ? "" : employeeSearchParameters.getEmail());
            query.setParameter("firstName", employeeSearchParameters.getFirstName() == null ? "" : employeeSearchParameters.getFirstName());
            query.setParameter("lastName", employeeSearchParameters.getLastName() == null ? "" : employeeSearchParameters.getLastName());
            query.setParameter("phone", employeeSearchParameters.getPhone() == null ? "" : employeeSearchParameters.getPhone());

            return query.list();
        }
    }

    @Override
    public Employee getById(Long id) {
        return super.getByField("id", id, Employee.class);
    }

    @Override
    public Employee getByEmail(String email) {
        return super.getByField("user.userCredentials.email", email, Employee.class);
    }

    @Override
    public Employee getByPhone(String phone) {
        return super.getByField("user.phone", phone, Employee.class);
    }

    @Override
    public Employee create(Employee employee) {
        return super.create(employee);
    }

    @Override
    public Employee update(Employee employee) {
        return super.update(employee);
    }

    @Override
    public void delete(Long id) {
        super.delete(id, Employee.class);
    }

    @Override
    public boolean isEmployeeExisting(String fieldValue, String fieldName, Long id) {
        try {
            Employee employee = super.getByField(fieldName, fieldValue, Employee.class);
            if (id.equals(-1L) && employee != null) {
                return true;
            } else {
                return !employee.getId().equals(id);
            }
        } catch (EntityNotFoundException e) {
            return false;
        }
    }
}

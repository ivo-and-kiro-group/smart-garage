package com.telerikacademy.web.smartgarage.controllers.mvc;

import com.telerikacademy.web.smartgarage.exceptions.DeletingEntityException;
import com.telerikacademy.web.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.modelmapper.VehicleModelMapper;
import com.telerikacademy.web.smartgarage.models.Vehicle;
import com.telerikacademy.web.smartgarage.models.VehicleDto;
import com.telerikacademy.web.smartgarage.models.VehicleType;
import com.telerikacademy.web.smartgarage.models.searchparameters.ModelSearchParameters;
import com.telerikacademy.web.smartgarage.models.searchparameters.VehicleSearchParameters;
import com.telerikacademy.web.smartgarage.repositories.contracts.VehicleTypeRepository;
import com.telerikacademy.web.smartgarage.services.contracts.ModelService;
import com.telerikacademy.web.smartgarage.services.contracts.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
@RequestMapping("/vehicles")
public class VehicleMvcController {

    private final VehicleService vehicleService;
    private final VehicleModelMapper modelMapper;
    private final ModelService modelService;
    private final VehicleTypeRepository vehicleTypeRepository;

    @Autowired
    public VehicleMvcController(VehicleService vehicleService,
                                VehicleModelMapper modelMapper,
                                ModelService modelService,
                                VehicleTypeRepository vehicleTypeRepository) {
        this.vehicleService = vehicleService;
        this.modelMapper = modelMapper;
        this.modelService = modelService;
        this.vehicleTypeRepository = vehicleTypeRepository;
    }

    @ModelAttribute("models")
    public List<com.telerikacademy.web.smartgarage.models.Model> models() {
        return modelService
                .getAll(new ModelSearchParameters())
                .stream()
                .sorted(Comparator.comparing(m -> m.getManufacturer().getName()))
                .collect(Collectors.toList());
    }

    @ModelAttribute("types")
    public List<VehicleType> types() {
        return vehicleTypeRepository.getAll();
    }

    @GetMapping
    @PreAuthorize("hasRole('EMPLOYEE')")
    public String showAllVehicles(Model model,
                                  @RequestParam("page") Optional<Integer> page,
                                  @RequestParam("size") Optional<Integer> size) {

        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);

        Page<Vehicle> vehiclePage = vehicleService.findPaginated(PageRequest.of(currentPage - 1, pageSize));
        List<Vehicle> vehicleList = vehiclePage.getContent();

        model.addAttribute("vehicleList", vehicleList);
        model.addAttribute("vehicleSearchParameters", new VehicleSearchParameters());
        model.addAttribute("vehiclePage", vehiclePage);

        int totalPages = vehiclePage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }

        return "vehicles";
    }

    @GetMapping("/search")
    @PreAuthorize("hasRole('EMPLOYEE')")
    public String handleSearchGetVehicles() {
        return "redirect:/vehicles";
    }

    @PostMapping("/search")
    public String handleVehiclesSearch(Model model,
                                       @ModelAttribute("vehicleSearchParameters")
                                               VehicleSearchParameters vehicleSearchParameters) {

        model.addAttribute("vehicleList", vehicleService.getAll(vehicleSearchParameters));

        return "vehicles";
    }

    @GetMapping("/create")
    @PreAuthorize("hasRole('EMPLOYEE')")
    public String showCreateVehiclePage(Model model) {
        model.addAttribute("vehicle", new VehicleDto());
        return "vehicle-new";
    }


    @PostMapping("/create")
    public String handleCreateVehiclePage(@Valid @ModelAttribute("vehicle") VehicleDto vehicleDto,
                                          BindingResult errors) {

        if (errors.hasErrors()) {
            return "vehicle-new";
        }

        try {
            Vehicle vehicle = modelMapper.fromDto(vehicleDto);
            vehicleService.create(vehicle);
            return "redirect:/vehicles";
        } catch (DuplicateEntityException e) {
            if (e.getMessage().contains("license plate")) {
                errors.rejectValue("licensePlate", "Duplicated License Plate", e.getMessage());
            } else if (e.getMessage().contains("vin")) {
                errors.rejectValue("vin", "Duplicated VIN", e.getMessage());
            } else if (e.getMessage().contains("email")) {
                String msg = String.format("Customer with email %s not found.", vehicleDto.getEmail());
                errors.rejectValue("email", "invalid_email", msg);
            }
            return "vehicle-new";
        } catch (EntityNotFoundException e) {
            if (e.getMessage().contains("license plate")) {
                errors.rejectValue("licensePlate", "Duplicated License Plate", e.getMessage());
            } else if (e.getMessage().contains("email")) {
                String msg = String.format("Customer with email %s not found.", vehicleDto.getEmail());
                errors.rejectValue("email", "invalid_email", msg);
            }
            return "vehicle-new";
        }
    }

    @GetMapping("/{id}/update")
    @PreAuthorize("hasRole('EMPLOYEE')")
    public String showVehicleEdit(@PathVariable Long id, Model model) {
        try {
            Vehicle vehicle = vehicleService.getById(id);
            VehicleDto vehicleDto = modelMapper.toDto(vehicle);
            model.addAttribute("vehicle", vehicleDto);
            return "vehicle-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @PostMapping("/{id}/update")
    public String editVisit(@PathVariable Long id,
                            @Valid @ModelAttribute("vehicle") VehicleDto vehicleDto,
                            BindingResult errors) {

        if (errors.hasErrors()) {
            return "vehicle-update";
        }

        try {
            Vehicle vehicle = modelMapper.fromDto(vehicleDto, id);
            vehicleService.update(vehicle);
            return "redirect:/vehicles";
        } catch (DuplicateEntityException e) {
            if (e.getMessage().contains("license plate")) {
                errors.rejectValue("licensePlate", "Duplicated License Plate", e.getMessage());
            } else if (e.getMessage().contains("vin")) {
                errors.rejectValue("vin", "Duplicated VIN", e.getMessage());
            }
            return "vehicle-new";
        } catch (EntityNotFoundException e) {
            errors.rejectValue("customerEmail", "invalid_email", e.getMessage());
            return "vehicle-new";
        }
    }

    @GetMapping("/{id}/delete")
    @PreAuthorize("hasRole('EMPLOYEE')")
    public String deleteVehicle(@PathVariable Long id, Model model) {
        try {
            vehicleService.delete(id);
            return "redirect:/vehicles";
        } catch (EntityNotFoundException | DeletingEntityException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }
}

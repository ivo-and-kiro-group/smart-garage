package com.telerikacademy.web.smartgarage.controllers.mvc;

import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import com.telerikacademy.web.smartgarage.controllers.rest.CurrencyController;
import com.telerikacademy.web.smartgarage.models.Customer;
import com.telerikacademy.web.smartgarage.models.Service;
import com.telerikacademy.web.smartgarage.models.Visit;
import com.telerikacademy.web.smartgarage.services.contracts.CustomerService;
import com.telerikacademy.web.smartgarage.services.contracts.ServiceService;
import com.telerikacademy.web.smartgarage.services.contracts.VisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.List;

@Controller
@RequestMapping("/customers")
public class PDFController {

    private final ServletContext servletContext;
    private final TemplateEngine templateEngine;
    private final ServiceService serviceService;
    private final VisitService visitService;
    private final CustomerService customerService;
    private final CurrencyController currencyController;

    @Autowired
    public PDFController(ServletContext servletContext,
                         TemplateEngine templateEngine,
                         ServiceService serviceService,
                         VisitService visitService,
                         CustomerService customerService,
                         CurrencyController currencyController) {
        this.servletContext = servletContext;
        this.templateEngine = templateEngine;
        this.serviceService = serviceService;
        this.visitService = visitService;
        this.customerService = customerService;
        this.currencyController = currencyController;
    }

    @RequestMapping(path = "/pdf/{id}/{currency}")
    @PreAuthorize("hasRole('CUSTOMER') or hasRole('EMPLOYEE')")
    public ResponseEntity<?> getPDF(@PathVariable Long id,
                                    @PathVariable String currency,
                                    HttpServletRequest request,
                                    HttpServletResponse response) throws IOException {

        Visit visit = visitService.getById(id);
        List<Service> services = serviceService.getServicesByVisit(visit);
        services.forEach(e -> e.setPrice(currencyController.convert("BGN", currency,
                e.getPrice())));
        visit.setTotalPrice(currencyController.convert("BGN", currency, visit.getTotalPrice()));

        WebContext context = new WebContext(request, response, servletContext);
        context.setVariable("visitEntity", visit);
        context.setVariable("services", services);
        context.setVariable("currency", currency);
        String orderHtml = templateEngine.process("invoice", context);

        ByteArrayOutputStream target = new ByteArrayOutputStream();
        ConverterProperties converterProperties = new ConverterProperties();
        converterProperties.setBaseUri("http://localhost:8080");

        HtmlConverter.convertToPdf(orderHtml, target, converterProperties);

        byte[] bytes = target.toByteArray();

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=invoice.pdf")
                .contentType(MediaType.APPLICATION_PDF)
                .body(bytes);
    }

    public void test() throws IOException {
        File htmlSource = new File("input.html");
        File pdfDest = new File("output.pdf");
        // pdfHTML specific code
        ConverterProperties converterProperties = new ConverterProperties();
        HtmlConverter.convertToPdf(new FileInputStream(htmlSource),
                new FileOutputStream(pdfDest), converterProperties);
    }


}

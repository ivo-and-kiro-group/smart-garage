package com.telerikacademy.web.smartgarage.controllers.mvc;

import com.telerikacademy.web.smartgarage.controllers.rest.CurrencyController;
import com.telerikacademy.web.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.exceptions.LocalDateException;
import com.telerikacademy.web.smartgarage.models.*;
import com.telerikacademy.web.smartgarage.models.searchparameters.ServiceSearchParameters;
import com.telerikacademy.web.smartgarage.models.searchparameters.VehicleSearchParameters;
import com.telerikacademy.web.smartgarage.models.searchparameters.VisitSearchParameters;
import com.telerikacademy.web.smartgarage.repositories.contracts.StatusRepository;
import com.telerikacademy.web.smartgarage.services.contracts.CustomerService;
import com.telerikacademy.web.smartgarage.services.contracts.ServiceService;
import com.telerikacademy.web.smartgarage.services.contracts.VehicleService;
import com.telerikacademy.web.smartgarage.services.contracts.VisitService;
import com.telerikacademy.web.smartgarage.modelmapper.VisitModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
@RequestMapping("/visits")
public class VisitMvcController {

    private final VisitService visitService;
    private final ServiceService serviceService;
    private final VisitModelMapper modelMapper;
    private final VehicleService vehicleService;
    private final StatusRepository statusRepository;
    private final CurrencyController currencyController;
    private final CustomerService customerService;

    private static String currency = "BGN";

    @Autowired
    public VisitMvcController(VisitService visitService,
                              ServiceService serviceService,
                              VisitModelMapper modelMapper,
                              VehicleService vehicleService,
                              StatusRepository statusRepository,
                              CurrencyController currencyController,
                              CustomerService customerService) {
        this.visitService = visitService;
        this.serviceService = serviceService;
        this.modelMapper = modelMapper;
        this.vehicleService = vehicleService;
        this.statusRepository = statusRepository;
        this.currencyController = currencyController;
        this.customerService = customerService;
    }

    @ModelAttribute("currencyList")
    public List<String> getAllCurrencies() {
        return new ArrayList<>(currencyController.getAll().getSymbols().keySet());
    }

    @ModelAttribute("curr")
    public CurrencyDto setCurrency() {
        return new CurrencyDto(currency);
    }


    @ModelAttribute("vehicles")
    public List<Vehicle> vehicles() {
        return vehicleService.getAll(new VehicleSearchParameters());
    }

    @ModelAttribute("serviceList")
    public List<Service> services() {
        return serviceService.getAll(new ServiceSearchParameters());
    }

    @ModelAttribute("statuses")
    public List<Status> statuses() {
        return statusRepository.getAll();
    }

    @GetMapping
    @PreAuthorize("hasRole('CUSTOMER') or hasRole('EMPLOYEE')")
    public String showAllVisits(Model model,
                                @RequestParam("page") Optional<Integer> page,
                                @RequestParam("size") Optional<Integer> size) {

        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);

        Page<Visit> visitPage = visitService.findPaginated(PageRequest.of(currentPage - 1, pageSize),
                new VisitSearchParameters());
        List<Visit> visitList = visitPage.getContent();

        model.addAttribute("visitList", visitList);
        model.addAttribute("visitSearchParameters", new VisitSearchParameters());
        model.addAttribute("visitPage", visitPage);

        int totalPages = visitPage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }

        return "visits";
    }

    @GetMapping("/customer/{email}")
    public String getCustomerVisits(@PathVariable String email, @RequestParam("page") Optional<Integer> page,
                                    @RequestParam("size") Optional<Integer> size, Model model) {
        try {
            VisitSearchParameters visitSearchParameters = new VisitSearchParameters();
            visitSearchParameters.setCustomerEmail(email);
            int currentPage = page.orElse(1);
            int pageSize = size.orElse(5);

            Page<Visit> visitPage = visitService.findPaginated(PageRequest.of(currentPage - 1, pageSize),
                    visitSearchParameters);
            List<Visit> visitList = visitPage.getContent();

            model.addAttribute("visitList", visitList);
            model.addAttribute("visitSearchParameters", new VisitSearchParameters());
            model.addAttribute("visitPage", visitPage);

            int totalPages = visitPage.getTotalPages();
            if (totalPages > 0) {
                List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                        .boxed()
                        .collect(Collectors.toList());
                model.addAttribute("pageNumbers", pageNumbers);
            }
            return "customer-visits";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @GetMapping("/search")
    @PreAuthorize("hasRole('CUSTOMER') or hasRole('EMPLOYEE')")
    public String handleSearchGetVisits() {
        return "redirect:/visits";
    }

    @PostMapping("/search")
    public String handleVisitSearch(Model model,
                                    @ModelAttribute("visitSearchParameters") VisitSearchParameters visitSearchParameters) {

        model.addAttribute("visitList", visitService.getAll(visitSearchParameters));

        return "visits";
    }

    @PostMapping("/apply/currency/{id}")
    public String applyCurrency(@ModelAttribute("curr") CurrencyDto currency,
                                @PathVariable Long id, Model model) {
        Visit visit = visitService.getById(id);
        visit.setTotalPrice(currencyController.convert("BGN", currency.getName(),
                visit.getTotalPrice()));
        List<Service> services = serviceService.getServicesByVisit(visit);
        services.forEach(e -> e.setPrice(currencyController.convert("BGN", currency.getName(),
                e.getPrice())));
        model.addAttribute("visit", visit);
        model.addAttribute("services", services);
        VisitMvcController.currency = currency.getName();
        VisitMvcController.currency = "BGN";
        return "visit";
    }

    @GetMapping("/create")
    @PreAuthorize("hasRole('EMPLOYEE')")
    public String showCreateVisitPage(Model model) {
        model.addAttribute("visit", new VisitDto());
        return "visit-new";
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('CUSTOMER') or hasRole('EMPLOYEE')")
    public String showSingleVisit(@PathVariable Long id, Model model) {
        try {
            Visit visit = visitService.getById(id);
            List<Service> services = serviceService.getServicesByVisit(visit);
            model.addAttribute("visit", visit);
            model.addAttribute("services", services);
        } catch (EntityNotFoundException e) {
            return "redirect:/visits";
        }

        return "visit";
    }

    @PostMapping("/create")
    public String handleCreateVisitPage(@Valid @ModelAttribute("visit") VisitDto visitDto,
                                        BindingResult errors) {

        if (errors.hasErrors()) {
            return "visit-new";
        }

        try {
            Visit visit = modelMapper.fromDto(visitDto);
            visitService.create(visit);
            return "redirect:/visits";
        } catch (LocalDateException e) {
            errors.rejectValue("date", "invalid_date", e.getMessage());
            return "visit-new";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("licensePlate", "duplicate_licensePlate", e.getMessage());
            return "visit-new";
        } catch (EntityNotFoundException e) {
            errors.rejectValue("licensePlate", "invalid_licensePlate", e.getMessage());
            return "visit-new";
        }
    }

    @GetMapping("/{id}/update")
    @PreAuthorize("hasRole('EMPLOYEE')")
    public String showEditVisit(@PathVariable Long id, Model model) {
        try {
            Visit visit = visitService.getById(id);
            VisitDto visitDto = modelMapper.toDto(visit);
            model.addAttribute("visit", visitDto);
            return "visit-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @PostMapping("/{id}/update")
    public String editVisit(@PathVariable Long id,
                            @Valid @ModelAttribute("visit") VisitDto visitDto,
                            BindingResult errors) {

        if (errors.hasErrors()) {
            return "visit-update";
        }

        try {
            Visit visit = modelMapper.fromDto(visitDto, id);
            visitService.update(visit);
            return "redirect:/visits";
        } catch (LocalDateException e) {
            errors.rejectValue("date", "Invalid_date", e.getMessage());
            return "visit-update";
        } catch (MessagingException e) {
            return "error";
        } catch (EntityNotFoundException e) {
            errors.rejectValue("licensePlate", "invalid_licensePlate", e.getMessage());
            return "visit-update";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("licensePlate", "Invalid_licensePlate", e.getMessage());
            return "visit-update";
        }
    }

    @GetMapping("/{id}/delete")
    @PreAuthorize("hasRole('EMPLOYEE')")
    public String deleteVisit(@PathVariable Long id, Model model) {
        try {
            visitService.delete(id);
            return "redirect:/visits";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }
}

package com.telerikacademy.web.smartgarage.services.contracts;

import com.telerikacademy.web.smartgarage.models.Service;
import com.telerikacademy.web.smartgarage.models.Visit;
import com.telerikacademy.web.smartgarage.models.searchparameters.ServiceSearchParameters;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Set;

public interface ServiceService {

    List<Service> getAll(ServiceSearchParameters ssp);

    Page<Service> findPaginated(Pageable pageable);

    Service getById(Long id);

    List<Service> getServicesByVisit(Visit visit);

    Service getByName(String name);

    Service create(Service service);

    Service update(Service service);

    Double calcTotalPrice(Set<Service> services);

    void delete(Long id);
}

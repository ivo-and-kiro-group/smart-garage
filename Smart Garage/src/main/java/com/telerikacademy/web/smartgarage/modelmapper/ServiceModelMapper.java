package com.telerikacademy.web.smartgarage.modelmapper;

import com.telerikacademy.web.smartgarage.models.Service;
import com.telerikacademy.web.smartgarage.models.ServiceDto;
import com.telerikacademy.web.smartgarage.repositories.contracts.ServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ServiceModelMapper {

    private final ServiceRepository repository;

    @Autowired
    public ServiceModelMapper(ServiceRepository repository) {
        this.repository = repository;
    }

    public ServiceDto toDto(Service service) {
        ServiceDto serviceDto = new ServiceDto();
        serviceDto.setName(service.getName());
        serviceDto.setPrice(service.getPrice());
        return serviceDto;
    }

    public Service fromDto(ServiceDto serviceDto) {
        Service service = new Service();
        dtoToObject(serviceDto, service);
        return service;
    }

    public Service fromDto(ServiceDto serviceDto, Long id) {
        Service service = repository.getById(id);
        dtoToObject(serviceDto, service);
        return service;
    }

    private void dtoToObject(ServiceDto serviceDto, Service service) {
        service.setName(serviceDto.getName());
        service.setPrice(serviceDto.getPrice());
    }
}

package com.telerikacademy.web.smartgarage.controllers.rest;

import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.models.Service;
import com.telerikacademy.web.smartgarage.models.Visit;
import com.telerikacademy.web.smartgarage.models.VisitDto;
import com.telerikacademy.web.smartgarage.models.searchparameters.VisitSearchParameters;
import com.telerikacademy.web.smartgarage.services.contracts.ServiceService;
import com.telerikacademy.web.smartgarage.services.contracts.VisitService;
import com.telerikacademy.web.smartgarage.modelmapper.VisitModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.mail.MessagingException;
import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/api/visits")
public class VisitController {

    private final VisitService service;
    private final ServiceService serviceService;
    private final VisitModelMapper modelMapper;

    @Autowired
    public VisitController(VisitService service,
                           ServiceService serviceService,
                           VisitModelMapper modelMapper) {
        this.service = service;
        this.serviceService = serviceService;
        this.modelMapper = modelMapper;
    }

    @GetMapping
    @PreAuthorize("hasRole('EMPLOYEE')")
    public List<Visit> getAll(@RequestParam(required = false)
                              @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date,
                              @RequestParam(required = false) String licensePlate,
                              @RequestParam(required = false) String customerEmail,
                              @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate untilDate) {
        return service.getAll(new VisitSearchParameters(date, untilDate, licensePlate, customerEmail));
    }

    @GetMapping("/customer")
    @PreAuthorize("hasRole('CUSTOMER')")
    public List<Visit> forCustomerGetAll(@RequestParam(required = false)
                                         @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date,
                                         @RequestParam(required = false) String licensePlate,
                                         @RequestParam String customerEmail,
                                         @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate untilDate) {
        return service.getAll(new VisitSearchParameters(date, untilDate, licensePlate, customerEmail));
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('EMPLOYEE') or hasRole('CUSTOMER')")
    public Visit getById(@Valid @PathVariable Long id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/visit-services/{id}")
    @PreAuthorize("hasRole('EMPLOYEE') or hasRole('CUSTOMER')")
    public List<Service> getVisitServices(@PathVariable Long id) {
        Visit visit = getById(id);
        return serviceService.getServicesByVisit(visit);
    }

    @PostMapping
    @PreAuthorize("hasRole('EMPLOYEE')")
    public Visit create(@Valid @RequestBody VisitDto visitDto) {
        try {
            Visit visit = modelMapper.fromDto(visitDto);
            return service.create(visit);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('EMPLOYEE')")
    public Visit update(@Valid @PathVariable Long id, @Valid @RequestBody VisitDto visitDto) {

        Visit visit = modelMapper.fromDto(visitDto, id);
        try {
            return service.update(visit);
        } catch (MessagingException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('EMPLOYEE')")
    public void delete(@Valid @PathVariable Long id) {
        try {
            service.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}

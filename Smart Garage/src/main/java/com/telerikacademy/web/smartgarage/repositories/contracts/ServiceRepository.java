package com.telerikacademy.web.smartgarage.repositories.contracts;

import com.telerikacademy.web.smartgarage.models.Service;
import com.telerikacademy.web.smartgarage.models.Visit;
import com.telerikacademy.web.smartgarage.models.searchparameters.ServiceSearchParameters;

import java.util.List;

public interface ServiceRepository {

    List<Service> getAll(ServiceSearchParameters ssp);

    Service getById(Long id);

    List<Service> getServicesByVisit(Visit visit);

    Service getByName(String name);

    Service create(Service service);

    Service update(Service service);

    boolean isServiceExisting(String fieldValue, String fieldName, Long id);

    void delete(Long id);
}

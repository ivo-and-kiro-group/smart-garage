package com.telerikacademy.web.smartgarage.models.searchparameters;

public class ServiceSearchParameters {

    private String name;
    private Double fromPrice;
    private Double toPrice;

    public ServiceSearchParameters(String name, Double fromPrice, Double toPrice) {
        this.name = name;
        this.fromPrice = fromPrice;
        this.toPrice = toPrice;
    }

    public ServiceSearchParameters() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getFromPrice() {
        return fromPrice;
    }

    public void setFromPrice(Double fromPrice) {
        this.fromPrice = fromPrice;
    }

    public Double getToPrice() {
        return toPrice;
    }

    public void setToPrice(Double toPrice) {
        this.toPrice = toPrice;
    }
}

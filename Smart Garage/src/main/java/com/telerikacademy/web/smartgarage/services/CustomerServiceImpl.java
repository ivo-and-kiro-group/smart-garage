package com.telerikacademy.web.smartgarage.services;

import com.telerikacademy.web.smartgarage.config.SecurityConfig;
import com.telerikacademy.web.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.models.Customer;

import com.telerikacademy.web.smartgarage.models.Role;
import com.telerikacademy.web.smartgarage.models.Vehicle;
import com.telerikacademy.web.smartgarage.models.Visit;
import com.telerikacademy.web.smartgarage.models.searchparameters.CustomerSearchParameters;
import com.telerikacademy.web.smartgarage.repositories.contracts.CustomerRepository;
import com.telerikacademy.web.smartgarage.services.contracts.CustomerService;
import com.telerikacademy.web.smartgarage.services.contracts.MailSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Service
public class CustomerServiceImpl implements CustomerService {
    private final CustomerRepository repository;
    private final MailSender mailSender;

    @Autowired
    public CustomerServiceImpl(CustomerRepository repository, MailSender mailSender) {
        this.repository = repository;
        this.mailSender = mailSender;
    }

    @Override
    public List<Customer> getAll(CustomerSearchParameters customerSearchParameters) {
        return repository.getAll(customerSearchParameters);
    }

    @Override
    public Customer getById(Long id) {
        return repository.getById(id);
    }

    @Override
    public Customer getByPhone(String phone) {
        return repository.getByPhone(phone);
    }

    @Override
    public Customer getByEmail(String email) {
        return repository.getByEmail(email);
    }

    @Override
    public Customer create(Customer customer) throws MessagingException, UnsupportedEncodingException {
        validateDuplicate(customer, -1L);
        String newPassword = generateString();
        customer.getUser().getUserCredentials().setPassword(newPassword);
        encodePassword(customer);
        Customer newCustomer = repository.create(customer);
        mailSender.sendEmail(customer.getUser().getUserCredentials().getEmail(), newPassword);
        return newCustomer;
    }

    @Override
    public Customer update(Customer customer) {
        validateDuplicate(customer, customer.getId());
        if (customer.getUser().getUserCredentials().getPassword() != null
                && !customer.getUser().getUserCredentials().getPassword().equals("")
                && customer.getUser().getUserCredentials().getPassword().length() <= 20) {
            encodePassword(customer);
        }
        return repository.update(customer);
    }

    @Override
    public void delete(Long id) {
        repository.delete(id);
    }

    @Override
    public Page<Customer> findPaginated(Pageable pageable, CustomerSearchParameters customerSearchParameters) {

        List<Customer> allCustomers = getAll(customerSearchParameters);

        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<Customer> list;

        if (allCustomers.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, allCustomers.size());
            list = allCustomers.subList(startItem, toIndex);
        }

        return new PageImpl<Customer>(list, PageRequest.of(currentPage, pageSize), allCustomers.size());
    }

    private void encodePassword(Customer customer) {
        customer.getUser().getUserCredentials().setPassword(
                SecurityConfig.passwordEncoder().encode(customer.getUser().getUserCredentials().getPassword())
        );
    }

    private void validateDuplicate(Customer customer, Long id) {

        if (repository.isCustomerExisting(customer.getUser().getUserCredentials().getEmail(), "user.userCredentials.email", id)) {
            throw new DuplicateEntityException("Customer", "email", customer.getUser().getUserCredentials().getEmail());
        }

        if (repository.isCustomerExisting(customer.getUser().getPhone(), "user.phone", id)) {
            throw new DuplicateEntityException("Customer", "phone", customer.getUser().getPhone());
        }
    }

    private String generateString() {
        int leftLimit = 48; // numeral '0'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 10;
        Random random = new Random();

        return random.ints(leftLimit, rightLimit + 1)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }
}

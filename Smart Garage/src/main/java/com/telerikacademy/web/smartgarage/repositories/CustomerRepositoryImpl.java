package com.telerikacademy.web.smartgarage.repositories;

import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.models.Customer;
import com.telerikacademy.web.smartgarage.models.Vehicle;
import com.telerikacademy.web.smartgarage.models.Visit;
import com.telerikacademy.web.smartgarage.models.searchparameters.CustomerSearchParameters;
import com.telerikacademy.web.smartgarage.repositories.contracts.CustomerRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CustomerRepositoryImpl extends AbstractGenericCrudRepository<Customer> implements CustomerRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public CustomerRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Customer> getAll(CustomerSearchParameters customerSearchParameters) {
        try (Session session = sessionFactory.openSession()) {

            StringBuilder visitDateJoinQuery = new StringBuilder("");
            StringBuilder modelJoinQuery = new StringBuilder("");
            StringBuilder visitDateFilterQuery = new StringBuilder("");
            StringBuilder modelFilterQuery = new StringBuilder("");
            StringBuilder orderByQuery = new StringBuilder("");

            if (customerSearchParameters.getFromDate() != null && customerSearchParameters.getUntilDate() != null) {
                visitDateJoinQuery.append(" join vehicles v on v.customer_id = c.customer_id " +
                        " join visits vi on vi.vehicle_id = v.vehicle_id ");
                visitDateFilterQuery.append(" and visit_date between :fromDate and :untilDate ");
            }

            if (customerSearchParameters.getModelId() != null && customerSearchParameters.getModelId() != -1L) {
                if (customerSearchParameters.getFromDate() == null && customerSearchParameters.getUntilDate() == null) {
                    modelJoinQuery.append(" join vehicles v on v.customer_id = c.customer_id ");
                }
                modelFilterQuery.append(" and v.model_id = :modelId ");
            }

            if (customerSearchParameters.getOrderBy() != null && !customerSearchParameters.getOrderBy().equals("")) {
                String fieldName = customerSearchParameters.getOrderBy().equalsIgnoreCase("date") ?
                        " vi.visit_date " : "u." + customerSearchParameters.getOrderBy();

                if (fieldName.contains("date") && (visitDateJoinQuery.toString().equals("") && modelJoinQuery.toString().equals(""))) {
                    visitDateJoinQuery.append(" join vehicles v on v.customer_id = c.customer_id " +
                            " join visits vi on vi.vehicle_id = v.vehicle_id ");
                }
                orderByQuery.append(" order by ").append(fieldName);
            }

            Query<Customer> query = session.createNativeQuery(" select distinct c.customer_id, u.first_name, u.last_name," +
                    " u.phone, u.users_credentials_id, c.user_id, us.email " +
                    " from customers as c " +
                    " join users u on u.user_id = c.user_id " +
                    " join users_credentials us on us.users_credentials_id = u.users_credentials_id " +
                    visitDateJoinQuery +
                    modelJoinQuery +
                    " where u.first_name like concat('%', :firstName,'%') and " +
                    " u.last_name like concat('%', :lastName,'%') and " +
                    " u.phone like concat('%', :phone,'%') and " +
                    " us.email like concat ('%', :email,'%') " + visitDateFilterQuery + modelFilterQuery + orderByQuery, Customer.class);
            query.setParameter("firstName", customerSearchParameters.getFirstName() == null ? "" : customerSearchParameters.getFirstName());
            query.setParameter("lastName", customerSearchParameters.getLastName() == null ? "" : customerSearchParameters.getLastName());
            query.setParameter("email", customerSearchParameters.getEmail() == null ? "" : customerSearchParameters.getEmail());
            query.setParameter("phone", customerSearchParameters.getPhone() == null ? "" : customerSearchParameters.getPhone());

            if (customerSearchParameters.getFromDate() != null && customerSearchParameters.getUntilDate() != null) {
                query.setParameter("fromDate", customerSearchParameters.getFromDate());
                query.setParameter("untilDate", customerSearchParameters.getUntilDate());
            }

            if (customerSearchParameters.getModelId() != null && customerSearchParameters.getModelId() != -1L) {
                query.setParameter("modelId", customerSearchParameters.getModelId());
            }

            return query.list();
        }
    }

    @Override
    public <V> Customer getByField(String fieldName, V fieldValue, Class<Customer> clazz) {
        return super.getByField(fieldName, fieldValue, clazz);
    }

    @Override
    public Customer getById(Long id) {
        return super.getByField("id", id, Customer.class);
    }

    @Override
    public Customer getByEmail(String email) {
        return super.getByField("user.userCredentials.email", email, Customer.class);
    }

    @Override
    public Customer findByResetPassword(String token) {
        try (Session session = sessionFactory.openSession()) {
            var query = session.createQuery("from Customer as c " +
                    " where c.user.userCredentials.resetPassword like concat('%', :token, '%') ", Customer.class);
            query.setParameter("token", token);
            List<Customer> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Invalid Token!");
            }
            return result.get(0);
        }
    }

    @Override
    public Customer getByPhone(String phone) {
        return super.getByField("user.phone", phone, Customer.class);
    }

    @Override
    public Customer create(Customer customer) {
        return super.create(customer);
    }

    @Override
    public Customer update(Customer customer) {
        return super.update(customer);
    }

    @Override
    public void delete(Long id) {
        super.delete(id, Customer.class);
    }

    @Override
    public boolean isCustomerExisting(String fieldValue, String fieldName, Long id) {
        try {
            Customer customer = super.getByField(fieldName, fieldValue, Customer.class);
            if (id.equals(-1L) && customer != null) {
                return true;
            } else {
                return !customer.getId().equals(id);
            }
        } catch (EntityNotFoundException e) {
            return false;
        }
    }
}

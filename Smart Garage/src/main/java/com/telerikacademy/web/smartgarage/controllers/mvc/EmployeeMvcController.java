package com.telerikacademy.web.smartgarage.controllers.mvc;

import com.telerikacademy.web.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.models.Employee;
import com.telerikacademy.web.smartgarage.models.EmployeeDto;
import com.telerikacademy.web.smartgarage.models.searchparameters.EmployeeSearchParameters;
import com.telerikacademy.web.smartgarage.services.contracts.EmployeeService;
import com.telerikacademy.web.smartgarage.modelmapper.EmployeeModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
@RequestMapping("/employees")
public class EmployeeMvcController {

    private final EmployeeModelMapper employeeModelMapper;
    private final EmployeeService employeeService;

    private static EmployeeSearchParameters employeeSearchParameters = new EmployeeSearchParameters();

    @Autowired
    public EmployeeMvcController(EmployeeModelMapper employeeModelMapper, EmployeeService employeeService) {
        this.employeeModelMapper = employeeModelMapper;
        this.employeeService = employeeService;
    }

    @GetMapping
    @PreAuthorize("hasRole('ADMIN')")
    public String showAllEmployees(Model model,
                                   @RequestParam("page") Optional<Integer> page,
                                   @RequestParam("size") Optional<Integer> size) {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);

        if (!model.containsAttribute("employeeSearchParameters")) {
            model.addAttribute("employeeSearchParameters", new EmployeeSearchParameters());
        }

        Page<Employee> employeePage = employeeService.findPaginated(PageRequest.of(currentPage - 1, pageSize)
                , (EmployeeSearchParameters) model.getAttribute("employeeSearchParameters"));
        List<Employee> employeeList = employeePage.getContent();

        model.addAttribute("employeeList", employeeList);
        model.addAttribute("employeePage", employeePage);

        int totalPages = employeePage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }

        return "employees";
    }

    @GetMapping("/reset")
    @PreAuthorize("hasRole('ADMIN')")
    public String handleEmployeeReset(Model model) {
        return "redirect:/employees";
    }

    @PostMapping("/search")
    @PreAuthorize("hasRole('ADMIN')")
    public String handleEmployeeSearch(Model model, @ModelAttribute("employeeSearchParameters") EmployeeSearchParameters employeeSearchParameters) {
        model.addAttribute("employeeList", employeeService.getAll(employeeSearchParameters));
        return "employees";
    }

    @GetMapping("/create")
    @PreAuthorize("hasRole('ADMIN')")
    public String showCreateEmployeePage(Model model) {
        model.addAttribute("employee", new EmployeeDto());
        return "employee-new";
    }

    @PostMapping("/create")
    @PreAuthorize("hasRole('ADMIN')")
    public String handleCreateVisitPage(Model model,
                                        @Valid @ModelAttribute("employee") EmployeeDto employeeDto,
                                        BindingResult errors) {
        try {
            Employee employee = employeeModelMapper.fromDto(employeeDto);
            employeeService.create(employee);
            return "redirect:/employees";
        } catch (DuplicateEntityException e) {
            if (e.getMessage().contains("confirmEmail")) {
                errors.rejectValue("user.userCredentials.confirmEmail", "email_error", e.getMessage());
            }else if(e.getMessage().contains("email")){
                errors.rejectValue("user.userCredentials.email", "email_error", e.getMessage());
            } else {
                errors.rejectValue("user.phone", "phone_error", e.getMessage());
            }
            return "employee-new";
        }
    }

    @GetMapping("/{id}/update")
    @PreAuthorize("hasRole('ADMIN')")
    public String showEditProfilePage(@PathVariable Long id, Model model) {
        Employee employee;
        EmployeeDto employeeDto;
        try {
            employee = employeeService.getById(id);
            employeeDto = employeeModelMapper.toDto(employee);
        } catch (EntityNotFoundException e) {
            return "redirect:/employees";
        }
        model.addAttribute("employee", employeeDto);
        model.addAttribute("employeeId", employee.getId());
        return "employee-update";
    }

    @PostMapping("/{id}/update")
    @PreAuthorize("hasRole('ADMIN')")
    public String editEmployeeProfile(@Valid @ModelAttribute("employee") EmployeeDto employeeDto,
                                      @PathVariable Long id,
                                      BindingResult errors) {
        try {
            Employee employee = employeeModelMapper.fromDto(employeeDto, id);
            employeeService.update(employee);
        } catch (DuplicateEntityException e) {
            if (e.getMessage().contains("email")) {
                errors.rejectValue("user.userCredentials.email", "email_error", e.getMessage());
            } else {
                errors.rejectValue("user.phone", "phone_error", e.getMessage());
            }
            return "employee-update";
        } catch (EntityNotFoundException e) {
            return "error";
        }
        return "redirect:/employees";
    }

    @GetMapping("/{id}/delete")
    @PreAuthorize("hasRole('ADMIN')")
    public String deleteEmployee(@PathVariable Long id, Model model) {
        try {
            employeeService.delete(id);
            return "redirect:/employees";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }
}

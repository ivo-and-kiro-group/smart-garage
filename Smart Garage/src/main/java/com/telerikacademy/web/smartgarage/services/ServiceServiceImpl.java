package com.telerikacademy.web.smartgarage.services;

import com.telerikacademy.web.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.web.smartgarage.models.Service;
import com.telerikacademy.web.smartgarage.models.Visit;
import com.telerikacademy.web.smartgarage.models.searchparameters.ServiceSearchParameters;
import com.telerikacademy.web.smartgarage.repositories.contracts.ServiceRepository;
import com.telerikacademy.web.smartgarage.services.contracts.ServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.Collections;
import java.util.List;
import java.util.Set;

@org.springframework.stereotype.Service
public class ServiceServiceImpl implements ServiceService {

    private final ServiceRepository repository;

    @Autowired
    public ServiceServiceImpl(ServiceRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Service> getAll(ServiceSearchParameters ssp) {
        return repository.getAll(ssp);
    }

    @Override
    public Page<Service> findPaginated(Pageable pageable) {

        List<Service> allServices = getAll(new ServiceSearchParameters());

        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<Service> list;

        if (allServices.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, allServices.size());
            list = allServices.subList(startItem, toIndex);
        }

        return new PageImpl<>(list, PageRequest.of(currentPage, pageSize), allServices.size());
    }

    @Override
    public Service getById(Long id) {
        return repository.getById(id);
    }

    @Override
    public List<Service> getServicesByVisit(Visit visit) {
        return repository.getServicesByVisit(visit);
    }

    @Override
    public Service getByName(String name) {
        return repository.getByName(name);
    }

    @Override
    public Service create(Service service) {
        validateDuplicate(service, -1L);
        return repository.create(service);
    }

    @Override
    public Service update(Service service) {
        validateDuplicate(service, service.getId());
        return repository.update(service);
    }

    @Override
    public void delete(Long id) {
        repository.delete(id);
    }

    @Override
    public Double calcTotalPrice(Set<Service> services) {
        double totalPrice = 0;
        for (com.telerikacademy.web.smartgarage.models.Service service : services) {
            totalPrice += service.getPrice();
        }

        return totalPrice;
    }

    private void validateDuplicate(Service service, Long id) {
        if (repository.isServiceExisting(service.getName(), "name", id)) {
            throw new DuplicateEntityException("Service", "name", service.getName());
        }
    }
}

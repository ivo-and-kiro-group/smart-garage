package com.telerikacademy.web.smartgarage.models;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UserDto {

    @Valid
    @NotNull(message = "User credentials cannot be null.")
    private UserCredentialsDto userCredentials;

    @NotNull(message = "First name cannot be null.")
    @Size(min = 2, max = 20, message = "First name should be between 2 and 20 symbols")
    private String firstName;

    @NotNull(message = "Last name cannot be null.")
    @Size(min = 2, max = 20, message = "Last name should be between 2 and 20 symbols")
    private String lastName;

    @NotNull(message = "Phone number cannot be null.")
    @Size(min = 10, max = 13, message = "Phone number should be exactly 13 symbols")
    private String phone;

    public UserDto() {

    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public UserCredentialsDto getUserCredentials() {
        return userCredentials;
    }

    public void setUserCredentials(UserCredentialsDto userCredentials) {
        this.userCredentials = userCredentials;
    }
}
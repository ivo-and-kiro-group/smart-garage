package com.telerikacademy.web.smartgarage.models.searchparameters;

public class ModelSearchParameters {

    private String name;

    private String manufacturerName;

    public ModelSearchParameters(String name, String manufacturerName) {
        this.name = name;
        this.manufacturerName = manufacturerName;
    }

    public ModelSearchParameters() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }
}

package com.telerikacademy.web.smartgarage.controllers.rest;

import com.telerikacademy.web.smartgarage.models.Currency;
import com.telerikacademy.web.smartgarage.models.Rate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/api/currencies")
public class CurrencyController {


    private static final String URLCONVETER
            = "http://data.fixer.io/api/latest?access_key=f3cc90e3483c1c5699c053931cb5ce9f&" +
            "symbols=%s,%s&format=1";

    private static final String URLGETALLCURRENCIES =
            "http://data.fixer.io/api/symbols?access_key=f3cc90e3483c1c5699c053931cb5ce9f&";

    @GetMapping
    public Currency getAll() {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<Currency> request = new HttpEntity<>(new Currency());
        ResponseEntity<Currency> response = restTemplate
                .exchange(URLGETALLCURRENCIES, HttpMethod.GET, request, Currency.class);
        return response.getBody();
    }

    @GetMapping("/convert")
    public Double convert(@RequestParam String from, @RequestParam String to, @RequestParam Double amount) {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<Rate> request = new HttpEntity<>(new Rate());
        ResponseEntity<Rate> response = restTemplate
                .exchange(String.format(URLCONVETER, from, to), HttpMethod.GET, request, Rate.class);
        Rate rate = response.getBody();

        return roundAvoid(((amount / rate.getRates().get(from)) * rate.getRates().get(to)), 2);
    }

    private static double roundAvoid(double value, int places) {
        double scale = Math.pow(10, places);
        return Math.round(value * scale) / scale;
    }
}
package com.telerikacademy.web.smartgarage.modelmapper;

import com.telerikacademy.web.smartgarage.models.Manufacturer;
import com.telerikacademy.web.smartgarage.models.ManufacturerDto;
import com.telerikacademy.web.smartgarage.repositories.contracts.ManufacturerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ManufacturerModelMapper {
    private final ManufacturerRepository manufacturerRepository;

    @Autowired
    public ManufacturerModelMapper(ManufacturerRepository manufacturerRepository) {
        this.manufacturerRepository = manufacturerRepository;
    }

    public Manufacturer fromDto(ManufacturerDto manufacturerDto) {
        Manufacturer manufacturer = new Manufacturer();
        dtoToObject(manufacturerDto, manufacturer);
        return manufacturer;
    }

    public Manufacturer fromDto(ManufacturerDto manufacturerDto, Long id) {
        Manufacturer manufacturer = this.manufacturerRepository.getById(id);
        dtoToObject(manufacturerDto, manufacturer);
        return manufacturer;
    }

    public ManufacturerDto toDto(Manufacturer manufacturer) {
        ManufacturerDto manufacturerDto = new ManufacturerDto();
        manufacturerDto.setName(manufacturer.getName());
        return manufacturerDto;
    }

    private void dtoToObject(ManufacturerDto manufacturerDto, Manufacturer manufacturer) {
        manufacturer.setName(manufacturerDto.getName());
    }
}

package com.telerikacademy.web.smartgarage.services;

import com.telerikacademy.web.smartgarage.models.SmsRequest;
import com.telerikacademy.web.smartgarage.services.contracts.SmsSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

@org.springframework.stereotype.Service
public class SMSService {

    private final SmsSender smsSender;

    @Autowired
    public SMSService(@Qualifier("twilio") TwilioSmsSender smsSender) {
        this.smsSender = smsSender;
    }

    public void sendSms(SmsRequest smsRequest) {
        smsSender.sendSms(smsRequest);
    }
}

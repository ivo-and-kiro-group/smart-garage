package com.telerikacademy.web.smartgarage.modelmapper;

import com.telerikacademy.web.smartgarage.models.User;
import com.telerikacademy.web.smartgarage.models.UserCredentials;
import com.telerikacademy.web.smartgarage.models.UserCredentialsDto;
import com.telerikacademy.web.smartgarage.models.UserDto;
import com.telerikacademy.web.smartgarage.repositories.contracts.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserModelMapper {
    private final UserRepository userRepository;

    @Autowired
    public UserModelMapper(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User fromDto(UserDto userDto) {
        User user = new User();
        dtoToObject(userDto, user);
        return user;
    }

    public User fromDto(UserDto userDto, Long id) {
        User user = this.userRepository.getById(id);
        dtoToObjectUpdate(userDto, user);
        return user;
    }

    public UserDto toDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setUserCredentials(new UserCredentialsDto());
        userDto.getUserCredentials().setEmail(user.getUserCredentials().getEmail());
        userDto.getUserCredentials().setConfirmEmail(user.getUserCredentials().getEmail());
        userDto.getUserCredentials().setPassword("");
        userDto.getUserCredentials().setConfirmPassword("");
        userDto.setPhone(user.getPhone());
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        return userDto;
    }

    private void dtoToObject(UserDto userDto, User user) {
        user.setUserCredentials(new UserCredentials());
        user.getUserCredentials().setEmail(userDto.getUserCredentials().getEmail());
        user.getUserCredentials().setPassword(userDto.getUserCredentials().getPassword());
        user.setPhone(userDto.getPhone());
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
    }

    private void dtoToObjectUpdate(UserDto userDto, User user) {
        user.getUserCredentials().setEmail(userDto.getUserCredentials().getEmail());
        if(userDto.getUserCredentials().getPassword() != null
                /*!userDto.getUserCredentials().getPassword().equals("")*/){
            user.getUserCredentials().setPassword(userDto.getUserCredentials().getPassword());
        }
        user.setPhone(userDto.getPhone());
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
    }
}

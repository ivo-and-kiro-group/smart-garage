package com.telerikacademy.web.smartgarage.services;

import com.telerikacademy.web.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.web.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.web.smartgarage.models.Model;
import com.telerikacademy.web.smartgarage.models.searchparameters.ModelSearchParameters;
import com.telerikacademy.web.smartgarage.repositories.contracts.ModelRepository;
import com.telerikacademy.web.smartgarage.services.contracts.ModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class ModelServiceImpl implements ModelService {

    private final ModelRepository modelRepository;

    @Autowired
    public ModelServiceImpl(ModelRepository modelRepository) {
        this.modelRepository = modelRepository;
    }

    @Override
    public List<Model> getAll(ModelSearchParameters modelSearchParameters) {
        return modelRepository.getAll(modelSearchParameters);
    }

    @Override
    public Model getById(Long id) {
        return modelRepository.getById(id);
    }

    @Override
    public Model getByName(String name) {
        return modelRepository.getByName(name);
    }

    @Override
    public Model create(Model model) {
        validateDuplicate(model);
        return modelRepository.create(model);
    }

    @Override
    public Model update(Model model) {
        validateDuplicateUpdate(model);
        return modelRepository.update(model);
    }

    @Override
    public void delete(Long id) {
        modelRepository.delete(id);
    }

    @Override
    public Page<Model> findPaginated(Pageable pageable) {
        List<Model> allModels = getAll(new ModelSearchParameters());

        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<Model> list;

        if (allModels.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, allModels.size());
            list = allModels.subList(startItem, toIndex);
        }

        return new PageImpl<Model>(list, PageRequest.of(currentPage, pageSize), allModels.size());
    }

    private void validateDuplicate(Model model) {
        boolean duplicateNameExists = true;

        try {
            modelRepository.getByName(model.getName());
        } catch (EntityNotFoundException e) {
            duplicateNameExists = false;
        }

        if (duplicateNameExists) {
            throw new DuplicateEntityException("Model", "name", model.getName());
        }
    }

    private void validateDuplicateUpdate(Model model) {
        boolean duplicateNameExists = true;

        try {
            Model model1 = modelRepository.getByName(model.getName());
            if (model1.getId().equals(model.getId())) {
                duplicateNameExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateNameExists = false;
        }

        if (duplicateNameExists) {
            throw new DuplicateEntityException("Model", "name", model.getName());
        }
    }
}

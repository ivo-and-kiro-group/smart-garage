package com.telerikacademy.web.smartgarage.services;

import com.telerikacademy.web.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.web.smartgarage.exceptions.LocalDateException;
import com.telerikacademy.web.smartgarage.models.Customer;
import com.telerikacademy.web.smartgarage.models.SmsRequest;
import com.telerikacademy.web.smartgarage.models.Visit;
import com.telerikacademy.web.smartgarage.models.searchparameters.VisitSearchParameters;
import com.telerikacademy.web.smartgarage.repositories.contracts.VisitRepository;
import com.telerikacademy.web.smartgarage.services.contracts.CustomerService;
import com.telerikacademy.web.smartgarage.services.contracts.MailSender;
import com.telerikacademy.web.smartgarage.services.contracts.ServiceService;
import com.telerikacademy.web.smartgarage.services.contracts.VisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

@Service
public class VisitServiceImpl implements VisitService {

    private final VisitRepository repository;
    private final ServiceService serviceService;
    private final SMSService smsService;
    private final CustomerService customerService;
    private final MailSender mailSender;


    @Autowired
    public VisitServiceImpl(VisitRepository repository,
                            ServiceService serviceService,
                            SMSService smsService,
                            CustomerService customerService,
                            MailSender mailSender) {
        this.repository = repository;
        this.serviceService = serviceService;
        this.smsService = smsService;
        this.customerService = customerService;
        this.mailSender = mailSender;
    }

    @Override
    public List<Visit> getAll(VisitSearchParameters vsp) {
        return repository.getAll(vsp);
    }

    @Override
    public Visit getById(Long id) {
        return repository.getById(id);
    }

    @Override
    public Visit getByDate(LocalDate date) {
        return repository.getByDate(date);
    }

    @Override
    public Visit create(Visit visit) {
        validateDate(visit.getDate());
        isVehicleExists(visit);
        Double totalPrice = serviceService.calcTotalPrice(visit.getServices());
        visit.setTotalPrice(totalPrice);

        return repository.create(visit);
    }

    @Override
    public Visit update(Visit visit) throws MessagingException {
        if (visit.getDate().isAfter(LocalDate.now())) {
            throw new LocalDateException("Date cannot be in the future.");
        }
        isVehicleExists(visit);
        Double totalPrice = serviceService.calcTotalPrice(visit.getServices());
        visit.setTotalPrice(totalPrice + visit.getTotalPrice());
        checkUpdateStatus(visit);
        return repository.update(visit);
    }

    @Override
    public void delete(Long id) {
        repository.delete(id);
    }

    @Override
    public Page<Visit> findPaginated(Pageable pageable, VisitSearchParameters vsp) {

        List<Visit> allVisits = getAll(vsp);

        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<Visit> list;

        if (allVisits.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, allVisits.size());
            list = allVisits.subList(startItem, toIndex);
        }

        return new PageImpl<>(list, PageRequest.of(currentPage, pageSize), allVisits.size());
    }

    private void validateDate(LocalDate date) {
        if (date.isBefore(LocalDate.now())) {
            throw new LocalDateException("Date cannot be in the past.");
        }

        if (date.isAfter(LocalDate.now())) {
            throw new LocalDateException("Date cannot be in the future.");
        }
    }

    private void checkUpdateStatus(Visit visit) throws MessagingException {
        if (visit.getStatus().getName().equals("Ready for pick up")) {
            smsService.sendSms(new SmsRequest(visit.getVehicle().getCustomer().getUser().getPhone(),
                    "Vui lek manqk brat ela da si vzemesh kolata! <3"));
        } else if (visit.getStatus().getName().equals("Finished")) {
            mailSender.sendEmailWithAttachment(visit.getVehicle().getCustomer().getUser().getUserCredentials().getEmail(),
                    "Hello, this is your invoice",
                    "Invoice", "" + visit.getId());
        }
    }

    private void isVehicleExists(Visit visit) {
        VisitSearchParameters vsp = new VisitSearchParameters();
        vsp.setLicensePlate(visit.getVehicle().getLicensePlate());
        List<Visit> visits = getAll(vsp);

        if (visits.size() != 0) {
            for (Visit v : visits) {
                if (!v.getStatus().getName().equals("Finished") && !v.getId().equals(visit.getId())) {
                    throw new DuplicateEntityException(String.format(
                            "Our mechanics are already working on vehicle with license plate %s",
                            visit.getVehicle().getLicensePlate()));
                }
            }
        }
    }
}

package com.telerikacademy.web.smartgarage.services.contracts;

import com.telerikacademy.web.smartgarage.models.Customer;
import com.telerikacademy.web.smartgarage.models.Vehicle;
import com.telerikacademy.web.smartgarage.models.Visit;
import com.telerikacademy.web.smartgarage.models.searchparameters.CustomerSearchParameters;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetailsService;

import javax.mail.MessagingException;
import java.io.UnsupportedEncodingException;
import java.util.List;

public interface CustomerService {
    List<Customer> getAll(CustomerSearchParameters customerSearchParameters);

    Customer getById(Long id);

    Customer getByEmail(String email);

    Customer getByPhone(String phone);

    Customer create(Customer customer) throws MessagingException, UnsupportedEncodingException;

    Customer update(Customer customer);

    void delete(Long id);

    Page<Customer> findPaginated(Pageable pageable, CustomerSearchParameters customerSearchParameters);
}

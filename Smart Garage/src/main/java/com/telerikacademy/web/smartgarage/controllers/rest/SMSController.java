package com.telerikacademy.web.smartgarage.controllers.rest;

import com.telerikacademy.web.smartgarage.services.SMSService;
import com.telerikacademy.web.smartgarage.models.SmsRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("api/v1/sms")
public class SMSController {

    private final SMSService SMSService;

    @Autowired
    public SMSController(SMSService SMSService) {
        this.SMSService = SMSService;
    }

    @PostMapping
    public void sendSms(@Valid @RequestBody SmsRequest smsRequest) {
        SMSService.sendSms(smsRequest);
    }
}

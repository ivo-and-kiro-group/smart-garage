# Smart Garage
`Created by Kiril Georgiev and Ivaylo Angelov` 
#### A web application that enables the owners of an auto repair shop to manage their day-to-day job.  The system has two parts:
* View Employee that can register customer visits
* View Customer that receives invoice, login information and view with the all services.
---
## Build
To run our project locally on Windows 10 environment using Git Bash 2.31.1, IntelliJ IDEA Ultimate 2020.3.1 and MariaDB Server 10.5.8, clone this repository.

* **Donwloading the project from GitLab**
    - Choose folder where you want to copy the project, then right click and select `"Git Bash Here"`:

    ![alt text](https://imgur.com/LA0ZLEg.png "Git Bash") 

    - After that you should go back to `GitLab` and clone the repository:
        - Select `'Clone'` then copy the URL:

    ![alt text](https://imgur.com/Qtzd8I0.png "Copy URL from GitLab")

    - Go back to GitBash write `"git clone"` and paste the URL:

    ![alt text](https://imgur.com/qv0wboM.png "Clone the repository")

    - You have already downloaded our project. The next step is to open it via some Java IDE, in this example we are using InteliJ IDEA Ultimate

* **Opening the project**

    - Go to InteliJ IDEA Welcome menu and click `"Open"`. Then choose the folder where you have downloaded the project:

        ![alt text](https://imgur.com/AYvzw76.png "Open the project")

        - After that you have to choose the folder `"Smart Garage"` with **`the small black square`** :

        ![alt text](https://imgur.com/nyIaFkz.png)

        - Great! The next step is to setup the project database.
* **Setting up the project database**
    - Find Database and click on it:
    ![alt text](https://imgur.com/obhEBI4.png "Find Database")

    - Then click on the small `"+"` sign and choose `"MariaDB"`

     ![alt text](https://imgur.com/lzyn11m.png "Choose MariaDB")
     - You will need to create a user  with password. The project uses smart-garage-user for username and the same for the password.

    ![alt text](https://imgur.com/vh5WE2A.png "smart-garage")

    - After that create a new schema. Rigth click on `"schemas"` and hit `"new"`.

    ![alt text](https://imgur.com/jWsR8Lz.png "craete new schema")    

    - Name it `"smart_garage"`.
    - Navigate to `"db"` package and open "db_create.sql". Select the whole script, right click into the file then hit `"Excecute"` and choose `"default"`

    ![alt text](https://imgur.com/wCyJVsJ.png "Execute sql script")

    - You can check the new schema

    ![alt text](https://imgur.com/jfRYwij.png "Check the new schema")

    - Now we need to fill it up with some data. Go back to `"db"` package and do the same as in the previous step with `"fill_db_script"` file. Make sure you have selected the whole script option before you click

    ![alt text](https://imgur.com/v2XtMwr.png "Fill DB") 

* **Database Relations**

    ![alt text](https://imgur.com/rLJFS00.png)

* **Using the System**

    - In this section we will discus how to use the system as an Employee. After you have run the scripts you already have two users with `Admin` and `Employee` roles. Run the application write `localhost:8080` in your favourite browser and hit `[Enter]`. 

    - Now you can see our site as a anonymous user.
    
         Snapshot 1

        ![alt text](https://imgur.com/uJGEinj.png "Home page")

        Snapshot 2
        ![alt text](https://imgur.com/lWYpptC.png)

    - Let's imagine that we have a new customer in our garage service. He does not have an account yet in our system. First step is to `Login` with account, which has employee role and `Create` a new customer in the system.

    - Navigate to the `"Login"` button. For `username` you can use **`"ivayloangelov@gmail.com"`** and **`password: pass1`** then click on the `Login` button. This account has 
    full rights even it can create new employees.

    ![alt text](https://imgur.com/OXqEILz.png "Login")


    - Now you can see `"Activities"` portal in the nav bar:

    ![alt text](https://imgur.com/7ESM2SV.png "Activities")

    - We said that the first step is to `Create(Register)` a new customer for this you have to click on `Create` button in `Customers` section:

    ![alt text](https://imgur.com/ChOEZ8m.png "Create Customer")

    - After you click `Create` you should be seeing this form:

     ![alt text](https://imgur.com/aElo2iw.png "Create form")

    - You should provide customer email, first name, last name and `phone(Note that it will be good to provide the same format as in the example in order to use one of the functionalities)`. Then click the `Submit` button.

    - After you click `Submit` you should be seeing table with all customers in the system and the newly created customer at the bottom.

    ![alt text](https://imgur.com/ZsKENF7.png "Newly created customer")

    - Our new customers receive email with randomized password and link for login immediately after you create their account. For example we created a `gmail` account in order to test the sending of email. It looks like this:

    ![alt text](https://imgur.com/ZyPoVGh.png "registration email")

    - As you can see we provide `Searching` by multiple criteria. Let's make one search by customer email, for example we will searching for the newly customer with email `p.penev792@gmail.com`:

    ![alt text](https://imgur.com/C6sZ0w0.png "Search")

    - You can `Seach` by visit date in range, by first name, last name and vehicle model/manufacturer. Also you can `Sort` by visit date(provide only `date` in the input field), first name(provide `first_name` in the input field) and last name(provide `last_name` in the input field). As you can see there is `ACTIONS` section for `Update` and `Delete`, you can `Update` a customer, but if you want to `Delete` you have to be sure that the given customer does not have linked with vehicles, if it has you have to delete them first.

    - Next step is to create pesonal vehicle linked to newly created customer. Go back to `Activities` and click on `Create` button in `Vehicles ` section.
    
    ![alt text](https://imgur.com/M33isf4.png "Create Vehicle") 

     - After you click `Create` for vehicles you should see `Create Vehicle` form:

    ![alt text](https://imgur.com/VV8jc3i.png "Create Vehicle form")

   - You have to provide vehicle `license plate`(should be between 6 and 8 symbols, if there is already vehicle with same license plate you will receive warning), `year`, `vehicle VIN`(should be exactly 17 symbols, otherwise you will receive warning), `customer email`(if there is no customer with same email you will receive warning), choose `Model`, `Manufacturer` and `Vehicle type`. Then click the `Submit` button.

   - After you click `Submit` you should be seeing table with all personal vehicles and the newly created at the bottom:

   ![alt text](https://imgur.com/4rJL9Kj.png "Vehicles table")

   - You can `Search` by `license plate` and `customer email` as well. As you can see these is `ACTIONS` section for `UPDATE` and `DELETE`, you can update a given vehicle, but if want to `Delete` a vehicle you have to be sure that this vehicle does not have visits, if it has you have to delete them first.

   - Next and final step of customer registration is to create a visit in our system. Go back to `Activities` and select `Create` in `Visits` section. The same way as the previous two steps. 

   ![alt text](https://imgur.com/8Hk251k.png "Activities") 

   - You should provide `Date`, `License plate`(Note that if there is already registered visit with same license plate and visit status different from `Finished`, you will receive warning), `Status` and `Services` with multiple selection then click `Submit`:

   ![alt text](https://imgur.com/a3Jdc5R.png "Create a visit")

   - After you click `Submit` you should be seeing table with all Vehicles:

   ![alt text](https://imgur.com/0X27cpp.png "All visits")

   - Again we provided `Search` by multiple criteria, `Update` and `Delete` buttons as well.

   - The work of the our employees is to `Update` the created visits. When our mechanics are done with a given vehicle the employee has to `Update` the visit linked with this vehicle. For example will go to `All Visits` and `Search` by `license plate`. After that it has to `Update` the searched visit through the `Update` button:

   ![alt text](https://imgur.com/YWNqVmh.png "Update a visit")

   - Next step is to change the `Status` to `Ready for pick up` and click `Sybmit`:

   ![alt text](https://imgur.com/d4Kk5wm.png "Update visit form")

   - After the employee change the status to `Ready for pick up` our customer receives a SMS on his mobile phone.

   -  When our customer sees the SMS, he understands that his car is ready for pick up. When the customer wants to pay, our employee has to send invoice to the customer email. For this purpose we will go back to `All Visits` and use `Search` functionality in order to find the visit of our customer and vehicle. Then click on the hyper link which is the `ID`:

   ![alt text](https://imgur.com/MBksiJI.png "Search visit")

   - After you click on the `Hyper link` you should be seeing full details for the given visit:

   ![alt text](https://imgur.com/CPEvOLi.png "Single visit")

   - Below you can choose `currency`. By default it is `Bulgarian Leva`. If the customer wants a different currency, you have to find the wanted currency in the dropdown menu then click `Save`:

   ![alt text](https://imgur.com/9M6WG6B.png "Default currency")

    - For example we will choose `EUR`:

    ![alt text](https://imgur.com/EKQX8De.png "Choose EUR")

    - After that the employee has to click on `Invoice`. The button will generate locally invoice pdf file. Then the employee has to go back to `All Visits`, find the same visit and change the status of the given visit.

    - Go to Activities->All-Visits and use `Search` functionality in order to find the visit by license plate then click `Update`:

    ![alt text](https://imgur.com/ON0s8ly.png "Update visit")

    - Change the status from `Ready for pick up` to `Finished` click `Submit` and our customer will receive on the email invoice in `PDF` format:

    - The mail looks like this:

    ![alt text](https://imgur.com/EyMJVsS.png "Email with PDF")

    - The invoice:

    ![alt text](https://imgur.com/PXRmSSk.png "Invoice")

    - When a customer `Login` first he can `Change` the randomly password through the `Profile` portal. As well he can see its all visits in our service. If a customer losts the original invoice he can generate a copy as he can choose different currency from the default `BGN`. 





